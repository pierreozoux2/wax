/* eslint react/prop-types: 0 */

import { clone } from 'lodash'

import {
  Component,
  // FontAwesomeIcon as Icon
} from 'substance'

import CommentList from './CommentList'

class CommentBox extends Component {
  constructor(props, args) {
    super(props, args)
    this.inputHeight = 0

    this.editComment = this.editComment.bind(this)

    this.state = {
      textAreaFocused: false,
    }
  }

  // TODO -- fix class names
  render($$) {
    const { active, discussion, entry, setTops } = this.props
    const name = this.props.user.username
    // let comments = this.props.comments.data
    let comments = clone(discussion)
    const replies = $$('span').addClass('total-replies')
    if (!active) {
      comments = discussion.slice(0, 1) // preview
      if (discussion.length === 0) return $$('div')
      if (discussion.length > 1) {
        let text = 'replies'
        if (discussion.length === 2) text = 'reply'
        replies.append(`${discussion.length - 1} ${text}`)
      }
    }

    const commentList = $$(CommentList, {
      active,
      comments,
      editComment: this.editComment,
      setTops,
    })

    const box = $$('li')
      .attr('data-comment', entry.id)
      .addClass('sc-comment-pane-item')
      .addClass('animation')
      .attr('data-clickable', true)
      .append(commentList)
      .append(replies)
      .on('click', this.makeActive)

    let resolve = $$('div')
    const placeholderMsg = 'add your comment here'

    if (active) {
      if (discussion.length > 0) {
        // const resolveIcon = $$(Icon, { icon: 'fa-check' })
        // placeholderMsg = 'add your comment here'

        resolve = $$('button')
          .attr('title', 'Resolve')
          .addClass('comment-resolve')
          .on('click', this.resolve)
      }

      const textarea = $$('textarea')
        .attr('rows', '1')
        .attr('placeholder', placeholderMsg)
        .ref('commentReply')
        .attr('id', entry.id)
        .on('keydown', this.onKeyDown)
        .on('input', this.textAreaAdjust)
        // .on('focus', this.resetEditableStateOnComments)
        .on('focus', this.focusTextArea)

      // const replyIcon = $$(Icon, { icon: 'fa-plus' })

      // TODO -- refactor classes!!!
      const reply = $$('button')
        .attr('disabled', true)
        .attr('title', 'Reply')
        .addClass('comment-reply')
        // .append(replyIcon)
        .ref('commentReplyButton')
        .on('click', this.reply)

      const nameEl = $$('div')
        .addClass('comment-user-name')
        .append(name)

      const inputBox = $$('div')
        .addClass('sc-new-comment')
        .append(nameEl, textarea, reply, resolve)

      box.addClass('sc-comment-active').append(inputBox)
    }

    return box
  }

  didUpdate() {
    const { setTops } = this.props
    setTops()
  }

  focusTextArea() {
    this.extendState({
      textAreaFocused: true,
    })
  }

  textAreaAdjust(event) {
    const { setTops } = this.props

    const value = this.refs.commentReply.getValue()
    const replyBtn = this.refs.commentReplyButton

    if (value.trim().length > 0) {
      replyBtn.el.removeAttr('disabled')
    } else {
      replyBtn.el.attr('disabled', 'true')
    }

    const textArea = event.path[0]

    textArea.style.height = '1px'
    textArea.style.height = `${textArea.scrollHeight}px`

    setTops()
  }

  reply() {
    const { discussion, entry } = this.props
    const id = entry.id
    const provider = this.getProvider()

    const user = this.props.user
    // const user = this.props.user.username

    const textarea = this.refs.commentReply
    const text = textarea.getValue()

    // TODO -- should I pass the whole user?
    discussion.push({
      text,
      user,
    })

    this.update(discussion)

    textarea.setValue('')
    this.rerenderBoxList()
    provider.focusEditor()
  }

  update(discussion) {
    const editorSession = this.getEditorSession()
    const id = this.props.entry.id

    editorSession.transaction((tx, args) => {
      const path = [id, 'discussion']
      tx.set(path, discussion)
    })
  }

  editComment(text, index) {
    const discussion = this.props.discussion
    const provider = this.getProvider()

    discussion[index].text = text
    this.update(discussion)
    provider.update()
  }

  makeActive() {
    const id = this.props.entry.id
    const provider = this.getProvider()

    const activeEntry = provider.getActiveEntry()
    if (activeEntry === id) return

    provider.setActiveEntry(id)
    provider.focusTextArea(id)
  }

  resolve() {
    const id = this.props.entry.id
    const provider = this.getProvider()

    provider.resolveComment(id)
  }

  getProvider() {
    return this.context.commentsProvider
  }

  getEditorSession() {
    return this.context.editorSession
  }

  // sends the event to trigger a rerender of the whole box list
  // cannot simply rerender this box, as its height might have changed
  // and the boxes underneath might need to move
  rerenderBoxList() {
    const provider = this.getProvider()
    provider.emit('commentReply:updated')
  }

  onKeyDown(e) {
    if (e.keyCode === 27) {
      // escape
      e.preventDefault()
      const provider = this.getProvider()

      provider.removeEmptyComments()
      provider.removeActiveEntry()
      provider.focusEditor()
    }

    if (e.keyCode === 13) {
      // enter
      e.preventDefault()

      const textAreaValue = this.refs.commentReply.getValue()
      if (textAreaValue.trim().length > 0) {
        this.reply()
      }
    }
  }
}

export default CommentBox
