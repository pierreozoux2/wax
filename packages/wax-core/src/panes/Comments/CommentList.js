/* eslint react/prop-types: 0 */

import { Component } from 'substance'

import Comment from './Comment'

class CommentList extends Component {
  constructor(props, args) {
    super(props, args)

    this.updateEditableComment = this.updateEditableComment.bind(this)

    this.state = {
      editableComment: null,
    }
  }

  updateEditableComment(index) {
    this.extendState({
      editableComment: index,
    })
  }

  render($$) {
    const { active, comments, editComment, setTops } = this.props
    const { editableComment } = this.state

    const commentsEl = comments.map((comment, index) =>
      $$(Comment, {
        active,
        index,
        editableComment,
        editComment,
        setTops,
        text: comment.text,
        updateEditableComment: this.updateEditableComment,
        user: comment.user,
      }),
    )

    return $$('div')
      .addClass('comment-list')
      .append(commentsEl)
  }
}

export default CommentList
