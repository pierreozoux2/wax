import { keys, map, groupBy, forEach, sortBy } from 'lodash'

import { HTMLExporter, Fragmenter } from 'substance'

import Article from './Article'

class Exporter extends HTMLExporter {
  constructor(config) {
    const converters = map(keys(config.converters.html), key => {
      return config.converters.html[key]
    })

    super({
      // schema: schema,
      converters,
      DocumentClass: Article,
    })
  }

  convertDocument(context, htmlEl) {
    const doc = context.editorSession.getDocument()
    const nodes = doc.getNodes()

    let el = ''
    const containersEls = []

    forEach(nodes, node => {
      if (node._isContainer) {
        containersEls.push(node)
      }
      if (
        (node.type === 'inline-note' && node.reference === '') ||
        node.type === 'find-and-replace'
      ) {
        doc.delete(node.id)
      }
    })

    const data = groupBy(containersEls, 'type')

    forEach(data, containers => {
      if (!containers[0].groupWithSameType) {
        el += this.createContent(containers)
      } else {
        el += `<div id="${containers[0].groupName}">`
        el += this.createContent(containers)
        el += `</div>`
      }
    })
    return el
  }

  createContent(containers) {
    let el = ''
    forEach(sortBy(containers, 'order'), container => {
      const content = this.convertContainer(container)
      let string = ''
      forEach(content, c => {
        string += c.outerHTML
      })
      el += `<${container.type} id="${container.id}">${string}</${
        container.type
      }>`
    })
    return el
  }

  _annotatedText(text, annotations) {
    var self = this

    var annotator = new Fragmenter()
    annotator.onText = function(context, text) {
      context.children.push(text)
    }
    annotator.onEnter = function(fragment) {
      var anno = fragment.node
      return {
        annotation: anno,
        children: [],
      }
    }
    annotator.onExit = function(fragment, context, parentContext) {
      var anno = context.annotation
      var converter = self.getNodeConverter(anno)
      if (!converter) {
        converter = self.getDefaultPropertyAnnotationConverter()
      }
      var el
      if (converter.tagName) {
        el = this.$$(converter.tagName)
      } else {
        el = this.$$('span')
      }
      el.attr(this.config.idAttribute, anno.id)
      el.append(context.children)
      if (converter.export) {
        el = converter.export(anno, el, self) || el
      }
      parentContext.children.push(el)
    }.bind(this)
    var wrapper = { children: [] }
    annotator.start(wrapper, text, annotations)
    return wrapper.children
  }
}

export default Exporter
