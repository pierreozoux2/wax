import SimpleBar from 'simplebar'
import 'simplebar/dist/simplebar.css'
import {
  Component,
  // ContextMenu,
  Overlay,
  ScrollPane,
  SplitPane,
} from 'substance'

import Comments from '../../panes/Comments/CommentBoxList'
import ContainerEditor from '../../ContainerEditor'
import DiacriticsModal from '../../elements/diacritics/DiacriticsModal'
import FindAndReplaceModal from '../../elements/find_and_replace/FindAndReplaceModal'
import ShortCutsModal from '../../elements/shortcuts_modal/ShortCutsModal'
import ModalWarning from '../../elements/modal_warning/ModalWarning'
import Notes from '../../panes/Notes/Notes'
import SideToolbar from '../SideToolbar'
import Toolbar from '../Toolbar'
import InfoPanelComponent from '../../panes/info_panel/InfoPanelComponent'
import './editoria.scss'

/*
  TODO -- refactor so that as much as default is re-used here
*/

class EditoriaLayout extends Component {
  render($$) {
    const {
      chapterNumber,
      changesNotSaved,
      commandStates,
      configurator,
      customTags,
      autoSave,
      containerId,
      editing,
      diacriticsModal,
      findAndReplaceModal,
      shortCutsModal,
      editorSession,
      onClose,
      spellCheck,
      trackChanges,
      trackChangesView,
      theme,
      update,
      user,
      tools,
    } = this.props

    const el = $$('div')

    // top toolbar
    const toolbar = $$(Toolbar, {
      commandStates,
      disabled: editing !== 'full',
      trackChanges,
      trackChangesView,
      configurator,
      tools,
    }).ref('toolbar')

    // left toolbar
    // TODO -- refactor sideToolbar and sideNav into one
    const sideToolbar = $$(SideToolbar, {
      configurator,
    }).ref('toolbarLeft')

    let chapterComponent = $$('div')
    if (chapterNumber) {
      const chapterNumberContent = `Chapter ${chapterNumber}`
      chapterComponent = $$('div')
        .append(chapterNumberContent)
        .addClass('sc-chapter-number')
    }
    const disabled = editing === 'disabled' ? true : false
    // surface
    const editor = $$(ContainerEditor, {
      containerId: 'main',
      editing,
      disabled,
      editorSession,
      autoSave,
      trackChanges,
      textCommands: ['full'],
    })
      .addClass(` wax-e-${theme}`)
      .ref('main')

    // notes pane
    const footerNotes = $$(Notes, {
      editing,
      trackChanges,
      trackChangesView,
      configurator,
      update,
      user,
    }).ref('footer-notes')

    const commentsPane = $$(Comments, {
      user,
      containerType: 'container',
      containerId: this.props.containerId,
    }).addClass('sc-comments-pane')

    const editorWithChapter = $$(SplitPane, {
      splitType: 'horizontal',
    }).append(chapterComponent, editor)

    // editor & comments split pane
    const editorWithComments = $$(SplitPane, {
      sizeA: '50%',
      splitType: 'vertical',
    }).append(editorWithChapter, commentsPane)

    const { overlay } = this.props.configurator.menus
    const overlayMenu = overlay ? overlay : 'defaultOverlay'

    // append overlay to editor/comments split pane
    const contentPanel = $$(ScrollPane, {
      name: 'contentPanel',
      scrollbarPosition: 'right',
    })
      .append(
        editorWithComments,
        $$(Overlay, {
          toolPanel: configurator.getToolPanel(overlayMenu),
          theme: 'dark',
        }),
      )
      .attr('id', `content-panel-${containerId}`)
      .ref('contentPanel')

    // editor/comments/sidebar/toolbar & notes pane
    const editorWithNotes = $$(SplitPane, {
      sizeA: '74%',
      splitType: 'horizontal',
    }).append(contentPanel, footerNotes)

    // editor/comments & side toolbar split pane
    const editorWithSidenav = $$(SplitPane, {
      sizeA: '16%',
      splitType: 'vertical',
    }).append(sideToolbar, editorWithNotes)

    // const contentPanelWithSplitPane = $$(SplitPane, {
    //   sizeA: '95%',
    //   splitType: 'vertical',
    // }).append(editorWithSidenav, $$('div'))

    // horizontal split pane with the toolbar and editor/comments/sidemenu
    const ToolbarWithEditor = $$(SplitPane, {
      splitType: 'horizontal',
    }).append(toolbar, editorWithSidenav)

    const infoPanel = $$('div')
      .addClass('info-panel')
      .append($$(InfoPanelComponent))

    const allEditor = $$('div').append(ToolbarWithEditor, infoPanel)
    // attach all to the element
    el.append(allEditor)

    // TODO -- centralize modals
    // unsaved changes modal
    const unsavedChangesModal = $$(ModalWarning, {
      action: '',
      location: '',
      onClose,
      textAlign: 'center',
      width: 'medium',
    }).ref('modal-warning')

    if (changesNotSaved) {
      el.append(unsavedChangesModal)
    }

    if (diacriticsModal) {
      el.append($$(DiacriticsModal))
    }

    if (findAndReplaceModal) {
      el.append($$(FindAndReplaceModal))
    }

    if (shortCutsModal) {
      el.append($$(ShortCutsModal))
    }

    return el
  }
}

export default EditoriaLayout
