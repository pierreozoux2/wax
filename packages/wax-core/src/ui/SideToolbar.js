import SimpleBar from 'simplebar'
import { Component, Toolbar } from 'substance'

class SideToolbar extends Component {
  didMount() {
    setTimeout(() => {
      this.sideBarPanel = new SimpleBar(document.getElementById('side-bar'))
    })
  }

  didUpdate() {
    this.sideBarPanel.init()
    this.sideBarPanel.initDOM()
  }

  render($$) {
    const { configurator } = this.props
    const displayMenu =
      configurator.menus && configurator.menus.sideToolBar
        ? configurator.menus.sideToolBar
        : 'sideDefault'

    const wrapper = $$('div')
      .addClass('sidenav')
      .attr('id', 'side-bar')
    const sideNav = $$('div')
      .addClass('se-toolbar-wrapper')

      .append(
        $$(Toolbar, {
          toolPanel: configurator.getToolPanel(displayMenu),
        }),
      )

    return wrapper.append(sideNav)
  }
}

export default SideToolbar
