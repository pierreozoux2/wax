import { platform } from 'substance'
import WaxAnnotationCommand from '../../commands/WaxAnnotationCommand'
import ChangeCaseTool from './ChangeCaseTool'
import ChangeCaseCommand from './ChangeCaseCommand'

export default {
  name: 'change-case',
  configure: config => {
    config.addCommand('change-case', ChangeCaseCommand, {
      commandGroup: 'change-case',
    })
    config.addTool('change-case', ChangeCaseTool)
  },
}
