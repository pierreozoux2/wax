import { TextBlock } from 'substance'

class Glossary extends TextBlock {}

Glossary.type = 'glossary'

export default Glossary
