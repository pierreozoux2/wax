import { AnnotationComponent } from 'substance'
import Subscript from './Subscript'
import SubscriptHTMLConverter from './SubscriptHTMLConverter'
import WaxAnnotationCommand from '../../../commands/WaxAnnotationCommand'

export default {
  name: 'subscript',
  configure: function(config) {
    config.addNode(Subscript)
    config.addConverter('html', SubscriptHTMLConverter)
    config.addComponent('subscript', AnnotationComponent)
    config.addCommand('subscript', WaxAnnotationCommand, {
      nodeType: 'subscript',
      commandGroup: 'annotations',
    })
    config.addIcon('subscript', { fontawesome: 'fa-subscript' })
    config.addLabel('subscript', {
      en: 'Subscript',
      de: 'Tiefgestellt',
    })
  },
  Subscript,
  SubscriptHTMLConverter,
}
