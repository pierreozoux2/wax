import { AnnotationComponent } from 'substance'

class StrongComponent extends AnnotationComponent {
  getTagName() {
    return 'strong'
  }
}

export default StrongComponent
