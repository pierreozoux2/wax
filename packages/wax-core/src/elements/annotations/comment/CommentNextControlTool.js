import { Tool } from 'substance'
import { FontAwesomeIcon as Icon } from 'substance'

class CommentNextControlTool extends Tool {
  getClassNames() {
    return 'commnent-next'
  }

  renderButton($$) {
    const el = super.renderButton($$)
    return el
  }
}

CommentNextControlTool.type = 'comment-next'

export default CommentNextControlTool
