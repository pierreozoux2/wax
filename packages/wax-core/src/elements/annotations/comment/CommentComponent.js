import { AnnotationComponent } from 'substance'

class CommentComponent extends AnnotationComponent {
  constructor(parent, props) {
    super(parent, props)

    const provider = this.getProvider()
    provider.on('comments:updated', this.rerender, this)
  }

  isActive() {
    const { node } = this.props
    return node.active
  }

  didUpdate() {
    // TODO -- try to remove timeout and still have overlapping comments work
    if (this.isActive()) {
      setTimeout(() => {
        this.el.addClass('sc-comment-active')
      })
    }
  }

  render($$) {
    const el = $$('span')
      .attr('data-id', this.props.node.id)
      .addClass(this.getClassNames())

    if (this.isActive()) {
      el.addClass('sc-comment-active')
    }

    el.append(this.props.children)
    // el.on('click', this.focus)

    return el
  }

  getProvider() {
    return this.context.commentsProvider
  }

  // focus () {
  //   var id = this.props.node.id
  //   var provider = this.context.commentsProvider
  //
  //   provider.focusTextArea(id)
  // }
}

export default CommentComponent
