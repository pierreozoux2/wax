/* eslint no-param-reassign: "off" */

export default {
  type: 'comment',
  tagName: 'comment',

  import: (element, node) => {
    node.discussion = JSON.parse(element.attr('discussion'))
  },

  export: (node, element) => {
    element.setAttribute('discussion', JSON.stringify(node.discussion))
  },
}
