import { PropertyAnnotation } from 'substance'

class Comment extends PropertyAnnotation {}

Comment.schema = {
  type: 'comment',
  discussion: {
    type: 'array',
    default: [],
  },
}

export default Comment
