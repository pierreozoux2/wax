import { Command } from 'substance'

class CommentPreviousNextCommand extends Command {
  getCommandState(params) {
    let newState = {
      active: false,
      disabled: true,
    }

    if (!params.surface) {
      return newState
    }

    const editorProps = params.surface.context.editor.props

    const provider = this.getProvider(params)
    const entries = provider.computeEntries()

    if (entries.length === 0) return newState

    newState = {
      active: true,
      disabled: false,
    }

    return newState
  }

  getCurrentSelection(params) {
    const editorSession = params.editorSession
    const selection = editorSession.getSelection()
    return selection
  }

  execute(params) {
    const provider = this.getProvider(params)
    const itemToFocus = this.getNextOrPrevious(params)
    provider.focus(itemToFocus)
  }

  getNextOrPrevious(params) {
    const provider = this.getProvider(params)
    const actionType = this.getActionType()

    if (actionType === 'previous') {
      return provider.getPreviousNode()
    }

    if (actionType === 'next') {
      return provider.getNextNode()
    }
    return false
  }

  getProvider(params) {
    return params.surface.context.commentsProvider
  }

  getAnnotationType() {
    return this.config.nodeType
  }

  getActionType() {
    return this.config.actionType
  }
}

export default CommentPreviousNextCommand
