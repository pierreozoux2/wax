import { AnnotationComponent } from 'substance'
import Superscript from './Superscript'
import SuperscriptHTMLConverter from './SuperscriptHTMLConverter'
import WaxAnnotationCommand from '../../../commands/WaxAnnotationCommand'

export default {
  name: 'superscript',
  configure: function(config) {
    config.addNode(Superscript)
    config.addConverter('html', SuperscriptHTMLConverter)
    config.addComponent('superscript', AnnotationComponent)
    config.addCommand('superscript', WaxAnnotationCommand, {
      nodeType: 'superscript',
      commandGroup: 'annotations',
    })
    config.addIcon('superscript', { fontawesome: 'fa-superscript' })
    config.addLabel('superscript', {
      en: 'Superscript',
      de: 'Hochgestellt',
    })
  },
  Superscript,
  SuperscriptHTMLConverter,
}
