import { AnnotationComponent } from 'substance'

class HighLighterComponent extends AnnotationComponent {
  render($$) {
    const el = super.render($$)
    const node = this.props.node
    const color = node.color
    el.addClass(`sc-highlighter-${color}`)
    return el
  }
}

export default HighLighterComponent
