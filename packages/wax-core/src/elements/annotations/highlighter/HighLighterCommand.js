import { Command, documentHelpers } from 'substance'

class HighLighterCommand extends Command {
  getCommandState(params) {
    let newState = {
      active: false,
      disabled: false,
    }

    const { surface } = params
    const selection = params.editorSession.getSelection()
    const highlightings = documentHelpers.getPropertyAnnotationsForSelection(
      params.editorSession.getDocument(),
      params.editorSession.getSelection(),
      { type: 'highlighter' },
    )

    if (surface && surface.props.textCommands) {
      if (
        !['full', this.name].some(e => {
          return surface.props.textCommands.indexOf(e) !== -1
        }) ||
        surface.props.editing === 'selection'
      ) {
        newState.disabled = true
        return newState
      }
    }

    if (
      (selection.isCollapsed() && highlightings.length === 0) ||
      !selection.isPropertySelection()
    ) {
      newState.disabled = true
    }

    if (params.surface) {
      const editorProps = params.surface.context.editor.props
      if (editorProps.editing === 'selection') newState.disabled = true
    }

    return newState
  }

  execute(params) {
    return true
  }
}

export default HighLighterCommand
