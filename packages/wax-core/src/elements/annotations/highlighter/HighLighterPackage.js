import HighLighter from './HighLighter'
import HighLighterComponent from './HighLighterComponent'
import HighLighterTool from './HighLighterTool'
import HighLighterCommand from './HighLighterCommand'
import HighLighterHTMLConverter from './HighLighterHTMLConverter'

export default {
  name: 'highlighter',
  configure: function(config) {
    config.addNode(HighLighter)
    config.addConverter('html', HighLighterHTMLConverter)
    config.addComponent(HighLighter.type, HighLighterComponent)
    config.addCommand('highlighter', HighLighterCommand, {
      spec: { type: 'highlighter' },
      commandGroup: 'highlighter',
    })
    config.addTool('highlighter', HighLighterTool)

    config.addLabel('highlighter', {
      en: 'highlighter',
    })
  },
}
