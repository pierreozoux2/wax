export default {
  type: 'highlighter',
  tagName: 'highlighter',

  import: (element, node) => {
    node.color = element.attr('color')
  },

  export: (node, element) => {
    element.setAttribute('color', node.color)
  },
}
