import { PropertyAnnotation } from 'substance'

class SmallCaps extends PropertyAnnotation {}

SmallCaps.schema = {
  type: 'small-caps',
}

export default SmallCaps
