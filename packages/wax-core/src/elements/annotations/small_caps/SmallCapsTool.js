import { Tool } from 'substance'

class SmallCapsTool extends Tool {
  renderButton($$) {
    let el = super.renderButton($$)
    el.addClass('toolbar-small-caps')
    return el
  }
}

export default SmallCapsTool
