import { Command } from 'substance'
import TrackChange from './TrackChange'
// TODO -- why command and not annotation command?

class TrackChangeControlCommand extends Command {
  getCommandState(params, config) {
    const newState = {
      disabled: false,
      active: false,
    }

    setTimeout(() => {
      if (params.surface && params.surface.props.trackChanges) {
        TrackChange.autoExpandRight = true
      } else {
        TrackChange.autoExpandRight = false
      }
    })

    return newState
  }

  execute(params, context) {
    const surface = context.surfaceManager.getFocusedSurface('main')
    const { editor } = surface.context

    const { trackChanges } = editor.props
    surface.send('update', { trackChanges: !trackChanges })
    editor.extendProps({ trackChanges: !trackChanges })
    if (!params.selection.isNull()) {
      params.editorSession.setSelection(params.selection)
    } else {
      surface.setFocusOnStartOfText()
    }
    editor.emit('TrackChangeToggle')

    return true
  }
}

TrackChangeControlCommand.type = 'track-change-enable'

export default TrackChangeControlCommand
