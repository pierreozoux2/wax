import { clone, each, filter, maxBy, minBy } from 'lodash'

import {
  getAllAnnotationsByStatus,
  isAnnotationFromTheSameUser,
} from './annotationHelpers'

import {
  getSelection,
  moveCursorTo,
  setSelectionPlusOne,
} from './selectionHelpers'

import {
  createTrackAnnotation,
  deleteSelection,
  insertText,
  removeTrackAnnotation,
  truncateTrackAnnotation,
} from './transformations'

const createAdditionAnnotationOnLastChar = options => {
  const { surface } = options
  options.selection = getSelection(surface)

  setSelectionPlusOne(options, 'left')
  createTrackAnnotation(options)
  moveCursorTo(options, 'end')
}

const deleteAllOwnAdditions = options => {
  const { selection, surface } = options
  const originalSelection = selection
  let shortenBy = 0

  const additions = getAllAnnotationsByStatus(options, 'add')

  const ownAdditions = filter(additions, annotation => {
    return isAnnotationFromTheSameUser(options, annotation)
  })

  each(ownAdditions, annotation => {
    const selection = annotation.getSelection()

    // make sure only the part of the annotation that is selected is deleted
    if (annotation.start.offset < originalSelection.start.offset) {
      selection.start.offset = originalSelection.start.offset
    }

    if (annotation.end.offset > originalSelection.end.offset) {
      selection.end.offset = originalSelection.end.offset
    }

    shortenBy += selection.end.offset - selection.start.offset
    const options = { selection, surface }
    deleteSelection(options)
  })

  return shortenBy // return how much shorter the selection should now be
}

const getProvider = options => {
  const { surface } = options
  return surface.context.trackChangesProvider
}

const insertCharacterWithoutExpandingAnnotation = (options, annotation) => {
  // After text has been inserted, set the event to null to make sure it does
  // not get added twice by a second iteration of the handlers.
  // eg. if at the end of the logic path, handleAddCollapsed gets called
  //     that will assume that there is text still to be inserted
  insertText(options)
  options.event = null

  // Clone the options, to make sure that the annotation or the selection
  // does not change while the truncate operation is in progress.
  const opts = clone(options)

  opts.selection = getSelection(opts.surface)
  opts.selection = setSelectionPlusOne(opts, 'left')
  opts.annotation = annotation
  opts.doc = opts.editorSession.getDocument() // TODO -- should get from handler

  truncateTrackAnnotation(opts)
  moveCursorTo(opts, 'end')
}

const selectCharacterAndMarkDeleted = options => {
  console.log('deprecated selectCharacterAndMarkDeleted')
  const { direction, surface } = options

  setSelectionPlusOne(options, direction.move)
  const selection = getSelection(surface)
  options.selection = selection

  options.status = 'delete'
  createTrackAnnotation(options)
  moveCursorTo(options, direction.cursorTo)
}

export {
  createAdditionAnnotationOnLastChar,
  deleteAllOwnAdditions,
  insertCharacterWithoutExpandingAnnotation,
  selectCharacterAndMarkDeleted,
}
