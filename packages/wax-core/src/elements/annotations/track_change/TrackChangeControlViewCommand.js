import { forEach } from 'lodash'
import { Command } from 'substance'

class TrackChangeControlViewCommand extends Command {
  getCommandState(params) {
    let newState = {
      active: false,
      disabled: false,
    }
    if (params.surface) {
      const { surface } = params
      const { context } = surface
      const { editor } = context
      const { props } = editor
      const { mode } = props

      if (!mode.trackChanges.view) {
        newState = {
          active: false,
          disabled: true,
        }
      }
    }

    return newState
  }

  // TODO -- review
  execute(params, context) {
    // TODO this works for now, review to find a better way
    const bodySurface = context.surfaceManager.getSurface('main')
    const surfaces = context.surfaceManager.surfaces
    bodySurface.send('trackChangesViewToggle')

    forEach(surfaces, surface => {
      surface.rerender()
    })

    // put the cursor back where it was on the surface
    if (!params.selection.isNull()) {
      params.editorSession.setSelection(params.selection)
    }

    return true
  }
}

TrackChangeControlViewCommand.type = 'track-change-toggle-view'

export default TrackChangeControlViewCommand
