import { Tool } from 'substance'

class TrackChangeAcceptControlTool extends Tool {
  getClassNames() {
    return 'track-change-accept'
  }

  renderButton($$) {
    const el = super.renderButton($$)
    const seperator = $$('span').addClass(
      'toolbar-track-changes-accept-seperator',
    )
    el.append('Accept', seperator)

    return el
  }
}

TrackChangeAcceptControlTool.type = 'track-change-accept'

export default TrackChangeAcceptControlTool
