import { PropertyAnnotation } from 'substance'

class TrackChange extends PropertyAnnotation {}

TrackChange.schema = {
  status: { type: 'string' },
  user: {
    type: 'object',
  },
}

TrackChange.type = 'track-change'

export default TrackChange
