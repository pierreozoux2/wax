export default {
  type: 'track-change',
  tagName: 'track-change',

  import: function(element, node, converter) {
    node.status = element.attr('status')
    node.user = {
      id: element.attr('user-id'),
      username: element.attr('username'),
      color: JSON.parse(element.attr('color')),
    }
  },

  export: function(node, element, converter) {
    const { status, user, oldType, addedType } = node

    element.setAttribute('status', status)
    element.setAttribute('user-id', user.id)
    element.setAttribute('username', user.username)
    if (user.color) element.setAttribute('color', JSON.stringify(user.color))
  },
}
