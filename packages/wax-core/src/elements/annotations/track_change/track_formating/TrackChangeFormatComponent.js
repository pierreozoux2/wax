import { forEach } from 'lodash'
import { AnnotationComponent, documentHelpers } from 'substance'

class TrackChangeFormatComponent extends AnnotationComponent {
  didMount() {
    // empty didMount as to not trigger
    // substance's listener
  }

  render($$) {
    const { id, status, user, addedType } = this.props.node
    const viewMode = this.getViewMode()
    const statusClass = `sc-track-change-${status}`
    const el = $$('span')
      .attr('data-id', id)
      .addClass(this.getClassNames())
      .addClass(statusClass)
      .append(this.props.children)

    let title = this.check(this.props.node)
    if (viewMode === false) {
      el.addClass('sc-track-format-hide')
    } else {
      el.attr('title', title)
    }
    return el
  }

  check(node) {
    let message = ''

    const changes = documentHelpers.getPropertyAnnotationsForSelection(
      this.context.editorSession.getDocument(),
      node.getSelection(),
      { type: 'track-change-format' },
    )

    if (node.addedType.length === 1) {
      message = `${node.addedType[0].username} added ${
        node.addedType[0].type
      }\n`
    }

    forEach(changes, (change, index) => {
      if (change.id === node.id) return
      if (
        (node.start.offset > change.start.offset ||
          node.start.offset === change.start.offset) &&
        change.addedType.length >= 1
      ) {
        message += `${change.addedType[0].username} added ${
          change.addedType[0].type
        }\n`
      }
    })

    if (node.oldType.length === 1) {
      message += `${node.oldType[0].username} removed ${node.oldType[0].type}\n`
    }

    forEach(changes, (change, index) => {
      if (change.id === node.id) return
      if (
        (node.start.offset > change.start.offset ||
          node.start.offset === change.start.offset) &&
        change.oldType.length >= 1
      ) {
        message += `${change.oldType[0].username} removed ${
          change.oldType[0].type
        }\n`
      }
    })

    return message
  }

  getEditor() {
    return this.context.editor
  }

  getProvider() {
    return this.context.trackChangesProvider
  }

  getViewMode() {
    const editor = this.getEditor()
    const { trackChangesView } = editor.state
    return trackChangesView
  }
}

export default TrackChangeFormatComponent
