import { AnnotationComponent, platform } from 'substance'
import Code from './Code'
import CodeHTMLConverter from './CodeHTMLConverter'
import WaxAnnotationCommand from '../../../commands/WaxAnnotationCommand'

export default {
  name: 'code',
  configure: config => {
    config.addNode(Code)
    config.addConverter('html', CodeHTMLConverter)
    config.addComponent('code', AnnotationComponent)
    config.addCommand('code', WaxAnnotationCommand, {
      nodeType: Code.type,
      commandGroup: 'insert-inline-code',
    })
    config.addIcon('code', { fontawesome: 'fa-code' })
    config.addLabel('code', {
      en: 'Code',
      de: 'Code',
    })
    let controllerKey = 'ctrl'
    if (platform.isMac) controllerKey = 'cmd'

    config.addKeyboardShortcut(`${controllerKey}+d`, {
      command: 'code',
    })
  },
}
