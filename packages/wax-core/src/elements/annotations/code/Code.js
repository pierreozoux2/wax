import { PropertyAnnotation } from 'substance'

class Code extends PropertyAnnotation {}

Code.type = 'code'

export default Code
