export default {
  type: 'script',
  tagName: 'script',

  import: function(el, node, converter) {
    node.source = el.attr('source')
    node.language = el.attr('language')
  },

  export: function(node, el) {
    el.attr('source', node.source)
    el.attr('language', node.language)
  },
}
