import { Tool } from 'substance'
import Script from './Script'
import ScriptEditor from './ScriptEditor'
import InsertScriptCommand from './InsertScriptCommand'
import ScriptHTMLConverter from './ScriptHTMLConverter'

export default {
  name: 'insert-script',
  configure: config => {
    config.addNode(Script)
    config.addComponent(Script.type, ScriptEditor)
    config.addCommand('insert-script', InsertScriptCommand, {
      nodeType: Script.type,
      commandGroup: 'insert-script',
    })
    config.addConverter('html', ScriptHTMLConverter)
    config.addTool('insert-script', Tool, { toolGroup: 'insert-script' })
    config.addIcon('insert-script', { fontawesome: 'fa-file-code-o' })
    config.addLabel('insert-script', 'Code Block')
  },
}
