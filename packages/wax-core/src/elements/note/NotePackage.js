import Note from './Note'
import NoteCommand from './NoteCommand'
import NoteComponent from './NoteComponent'
import NoteHTMLConverter from './NoteHTMLConverter'
import NoteTool from './NoteTool'
import NotesProvider from './NotesProvider'

export default {
  name: 'note',
  configure: function(config) {
    config.addNode(Note)

    config.addComponent(Note.type, NoteComponent)
    config.addConverter('html', NoteHTMLConverter)
    config.addCommand(Note.type, NoteCommand, {
      spec: { type: 'note' },
      commandGroup: 'note',
    })
    config.addTool('note', NoteTool)
    config.addIcon('note', { fontawesome: 'fa-bookmark' })
    config.addLabel('note', {
      en: 'Note',
    })

    config.addProvider('notesProvider', NotesProvider)
  },
}
