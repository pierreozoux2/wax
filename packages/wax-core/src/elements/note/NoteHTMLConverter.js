export default {
  type: 'note',
  tagName: 'note',

  import: (element, node, converter) => {
    node.disabled = JSON.parse(element.attr('disabled'))
  },

  export: (node, element, converter) => {
    const disabled = node.disabled
    element.setAttribute('disabled', disabled)
  },
}
