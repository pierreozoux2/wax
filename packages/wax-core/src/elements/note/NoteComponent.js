/* eslint react/prop-types: 0 */
import { Component } from 'substance'
import { findKey, forEach } from 'lodash'

class NoteComponent extends Component {
  didMount() {
    const provider = this.getProvider()
    provider.on('active:changed', this.rerender, this)
    provider.on('notes:updated', this.rerender, this)
  }

  render($$) {
    const { node } = this.props
    const viewMode = this.getViewMode()
    const provider = this.getProvider()
    const active = provider.getActiveNote()
    const entries = provider.entries
    const computedEntries = viewMode ? entries : this.excludeHidden(entries)

    const keyFound = findKey(computedEntries, note => note.id === node.id)
    const index = parseInt(keyFound, 10) + 1

    const noteContent = this.props.node['note-content']
    const number = $$('span')
      .addClass('sc-note-number')
      .append(index)

    const el = $$('span')
      .attr('note-content', noteContent)
      .addClass('sc-note')
      .append(number)
      .on('click', this.setActiveNote)

    if (node.id === active) el.addClass('active')

    return el
  }

  setActiveNote() {
    const node = this.props.node
    const provider = this.getProvider()
    provider.setActiveNote(node.id)
  }

  getProvider() {
    return this.context.notesProvider
  }

  getEditorSession() {
    return this.context.editorSession
  }

  dispose() {
    this.props.node.off(this)
  }

  getViewMode() {
    const editor = this.getEditor()
    const { trackChangesView } = editor.state
    return trackChangesView
  }

  getEditor() {
    return this.context.editor
  }

  excludeHidden(entries) {
    const filteredEntries = []
    forEach(entries, entry => {
      if (!entry.node.disabled) {
        filteredEntries.push(entry)
      }
    })
    return filteredEntries
  }
}

export default NoteComponent
