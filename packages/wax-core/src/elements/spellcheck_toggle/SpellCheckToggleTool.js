import { Tool } from 'substance'

class SpellCheckToggleTool extends Tool {
  renderButton($$) {
    let button = super.renderButton($$)

    const spellcheck = this.context.editor.state.spellCheck
    button.props.active = false
    if (spellcheck) {
      button.props.active = true
    }
    return button
  }
}

export default SpellCheckToggleTool
