import {
  BlockNodeComponent,
  deleteNode,
  FontAwesomeIcon as Icon,
} from 'substance'
import ContainerEditor from '../../ContainerEditor'

class ImageComponent extends BlockNodeComponent {
  didMount() {
    super.didMount.call(this)
    this.context.editorSession.onRender(
      'document',
      this._onDocumentChange,
      this,
    )
    this.context.editor.on('TrackChangeToggle', this.rerender, this)
  }

  dispose() {
    super.dispose.call(this)
    this.context.editorSession.off(this)
  }

  _onDocumentChange(change) {
    if (
      change.hasUpdated(this.props.node.id) ||
      change.hasUpdated(this.props.node.imageFile)
    ) {
      this.findContainer()
      // this.rerender()
    }
  }

  didUpdate() {
    this.findContainer()
  }
  click() {
    this.context.editorSession.transaction(tx => {
      tx.setSelection({
        type: 'node',
        containerId: 'main',
        surfaceId: 'main',
        nodeId: this.props.node.id,
        startOffset: 0,
        endOffset: 1,
      })
    })
    this.el.addClass('image-selected')
  }
  render($$) {
    const { editorSession, editor } = this.context
    const doc = editorSession.document
    const { trackChanges } = this.context.editor.props

    if (!doc.get(`caption-${this.props.node.id}`)) {
      doc.create({
        type: 'container',
        id: `caption-${this.props.node.id}`,
      })
    }

    let el = $$('figure')

    el.addClass('sc-image')
    el.append(
      $$('img')
        .on('click', this.click)
        .attr({
          src: this.props.node.getUrl(),
        })
        .ref('image'),
    )
    const { editing } = editor.props
    const disabled = editing === 'disabled' ? true : false

    const caption = $$(ContainerEditor, {
      containerId: `caption-${this.props.node.id}`,
      editorSession: this.context.editorSession,
      editing,
      disabled,
      trackChanges,
      textCommands: ['emphasis', 'diacritics', 'small-caps', 'comment'],
    }).ref(`caption-${this.props.node.id}`)

    const captionContainer = $$('div')
      .addClass('caption-container')
      .append(
        $$('span')
          .addClass('insert-caption')
          .append('Caption'),
      )

    captionContainer.append(caption)

    setTimeout(() => {
      editor.emit('ui:updated')
    }, 100)
    return el.append(captionContainer)
  }

  findContainer() {
    const { editorSession } = this.context

    const container = editorSession.surfaceManager.getSurface(
      `main/${this.props.node.id}/caption-${this.props.node.id}`,
    )

    if (container.isEmpty()) {
      container.createText()
    }
  }
}

export default ImageComponent
