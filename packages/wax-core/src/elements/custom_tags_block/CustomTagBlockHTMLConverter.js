export default {
  type: 'custom-block',
  tagName: 'custom-block',

  import: (el, node, converter) => {
    node.content = converter.annotatedText(el, [node.id, 'content'])
    node.class = el.attr('class')
  },

  export: (node, el, converter) => {
    el.setAttribute('class', node.class)
    el.append(converter.annotatedText([node.id, 'content']))
  },
}
