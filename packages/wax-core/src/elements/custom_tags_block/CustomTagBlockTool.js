import { forEach, first, last } from 'lodash'
import { Tool, FontAwesomeIcon as Icon } from 'substance'
import {
  createContainerSelection,
  containsList,
  isListItem,
  sliceListsAccordingToSelection,
  transactionHelpers,
  transformListToParagraphs,
} from '../../helpers/ListHelpers'

class CustomTagBlockTool extends Tool {
  render($$) {
    const { editorSession } = this.context
    const selection = editorSession.getSelection()

    let openCloseIcon = $$(Icon, { icon: 'fa-plus' })

    if (this.state.addNewTag) {
      openCloseIcon = $$(Icon, { icon: 'fa-minus' })
    }

    const Button = this.getComponent('button')
    let isDisabled = 'notDisabled'
    if (selection.containerId !== 'main' || selection.type === 'node')
      isDisabled = 'disabled'

    const el = $$('div')
      .addClass('custom-tags-container')
      .append(
        $$('span')
          .append('Custom')
          .append(openCloseIcon)
          .attr('title', 'add new style')
          .on('click', this.displayInsertTag),
      )
    const activeClass = this.getActiveTag(selection)
    const options = $$('div')
      .addClass('se-options')
      .ref('options')

    forEach(this.state.allCustomTags, (tag, index) => {
      let active = ''
      if (activeClass === tag.label) {
        active = 'sm-active'
      }
      let option = $$('div').append(
        $$('button')
          .attr(isDisabled, true)
          .addClass(active)
          .attr('data-type', tag.label)
          .append(
            $$('div')
              .append(tag.label)
              .addClass('option-box')
              .attr('title', tag.label),
          )
          .on('click', () => this.applyTransformation(tag.label)),
      )

      options.append(option)
    })

    const addNewCustomTag = $$('div')
      .addClass('new-custom-tag-container')
      .append(
        $$('input')
          .attr('id', 'addCustomTagBlock')
          .attr('placeholder', 'new custom tag')
          .attr('type', 'text')
          .addClass('input'),
      )
      .append(
        $$(Button, {
          label: 'add',
        })
          .addClass('new-custom-tag')
          .on('click', this.andNewTag),
      )
    if (this.state.addNewTag) {
      return el.append(addNewCustomTag, options)
    }
    return el.append(options)
  }

  applyTransformation(tag) {
    const { editorSession } = this.context
    const params = {
      surface: { containerId: 'main' },
      editorSession,
    }

    const selection = editorSession.getSelection()
    const isContainerSelection = selection.isContainerSelection()

    const selectionContainsList = containsList(params, selection)

    if (isContainerSelection) {
      if (selectionContainsList) {
        this.applyMixTransformations(params, selection, tag)
      } else {
        this.applyPropertyTransformations(params, selection, tag)
      }
      return
    }

    if (selectionContainsList) {
      this.applyMixTransformations(params, selection)
      return
    }

    editorSession.transaction(tx => {
      return tx.switchTextType({ type: 'custom-block', class: tag })
    })
  }

  applyPropertyTransformations(params, selection, tag) {
    const { editorSession } = params

    editorSession.transaction(tx => {
      this.createPropertyTransformations(params, tx, selection, tag)
    })
  }

  applyMixTransformations(params, selection, tag) {
    const { editorSession } = transactionHelpers(params)
    const isStartSelectionAListItem = isListItem(
      params,
      selection.start.path[0],
    )
    const isEndSelectionAListItem = isListItem(params, selection.end.path[0])
    const lists = sliceListsAccordingToSelection(params, selection)

    editorSession.transaction(tx => {
      const options = { config: { orderd: false } }
      const paragraphs = transformListToParagraphs(params, tx, lists, options)

      const startPath = isStartSelectionAListItem
        ? paragraphs[0]
        : selection.start.path[0]

      const endPath = isEndSelectionAListItem
        ? last(paragraphs)
        : selection.end.path[0]

      const sel = createContainerSelection(
        params,
        selection,
        tx,
        startPath,
        endPath,
      )

      this.createPropertyTransformations(params, tx, sel, tag)
    })
  }

  createPropertyTransformations(params, tx, selection, tag) {
    const { containerId } = transactionHelpers(params)

    const nodeData = { type: 'custom-block', class: tag }
    const newTransformations = []
    // Break selection spansning multiple nodes into pieces
    const propertySelections = selection.splitIntoPropertySelections()

    propertySelections.forEach(property => {
      // Make selection for block
      tx.setSelection({
        containerId,
        endOffset: property.end.offset,
        path: property.path,
        startOffset: property.start.offset,
        type: 'property',
      })

      // Apply style to block
      const transformation = tx.switchTextType(nodeData)

      // Keep new transformations ids to recreate selection
      newTransformations.push(transformation.id)
    })

    // Recreate Selection
    const firstBlock = first(newTransformations)
    const lastBlock = last(newTransformations)

    tx.setSelection({
      containerId,
      endPath: [lastBlock, 'content'],
      endOffset: selection.end.offset,
      startPath: [firstBlock, 'content'],
      startOffset: selection.start.offset,
      type: 'container',
    })
  }

  getActiveTag(selection) {
    if (
      selection.isNull() ||
      selection.isContainerSelection() ||
      selection.type === 'node'
    )
      return

    const { editorSession } = this.context
    const doc = editorSession.document
    const node = doc.get(selection.path[0])
    if (node.type === 'custom-block') return node.class
    return ''
  }

  displayInsertTag() {
    this.extendState({ addNewTag: !this.state.addNewTag })
  }

  andNewTag() {
    const { update } = this.context.editor.props

    const customTag = document.getElementById('addCustomTagBlock').value

    const addNewTag = { label: customTag, tagType: 'block' }
    this.state.allCustomTags.push(addNewTag)

    this.extendState({
      allCustomTags: this.state.allCustomTags,
    })

    update({ tags: [addNewTag] })
  }

  getInitialState() {
    const { editor: { props: { customTags: { block = [] } } } } = this.context 
    return {
      allCustomTags: block,
      addNewTag: false,
    }
  }
}

export default CustomTagBlockTool
