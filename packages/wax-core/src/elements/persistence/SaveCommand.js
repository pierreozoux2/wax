import { Command } from 'substance'

class SaveCommand extends Command {
  constructor() {
    super({ name: 'save' })
  }

  getCommandState(params) {
    const dirty = params.editorSession.hasUnsavedChanges()
    const autoSave = params.editorSession.configurator.config.autoSave
    // const disabled = autoSave ? false : !dirty
    return {
      disabled: false,
      active: false,
    }
  }

  execute(params) {
    const editorSession = params.editorSession
    const dirty = params.editorSession.hasUnsavedChanges()
    editorSession.save()
    setTimeout(() => {
      params.surface.context.editor.emit('hightLightSave')
    }, 200)

    // if (dirty)
    return {
      status: 'saving-process-started',
    }
  }
}

export default SaveCommand
