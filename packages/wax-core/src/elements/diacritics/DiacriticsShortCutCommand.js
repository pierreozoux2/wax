import { Command } from 'substance'

class DiacriticsShortCutCommand extends Command {
  getCommandState(params) {
    const sel = params.selection
    const { surface } = params
    const newState = {
      disabled: false,
      active: true,
    }

    return newState
  }

  execute(params, context) {
    const character = this.getCharacter()
    const { editorSession } = params
    const bodySurface = context.surfaceManager.getSurface('main')
    const { editor } = bodySurface.context
    const { trackChanges } = editor.props
    const { user } = editor.props
    const selection = editorSession.getSelection()

    if (trackChanges) {
      editorSession.transaction(
        (tx, args) => {
          tx.insertText(character)
          tx.create({
            status: 'add',
            type: 'track-change',
            path: selection.start.path,
            startOffset: selection.start.offset,
            endOffset: selection.end.offset + 1,
            user,
          })
        },
        { action: 'type' },
      )
      return
    }

    editorSession.transaction(
      (tx, args) => {
        tx.insertText(character)
      },
      { action: 'type' },
    )
  }

  getCharacter() {
    return this.config.character
  }
}

export default DiacriticsShortCutCommand
