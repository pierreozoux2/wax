import { platform } from 'substance'
import SwitchSurfaceCommand from './SwitchSurfaceCommand'

export default {
  name: 'switch-surface',
  configure: config => {
    config.addCommand('switch-surface', SwitchSurfaceCommand)

    let controllerKey = 'ctrl'
    if (platform.isMac) controllerKey = 'cmd'

    config.addKeyboardShortcut(`${controllerKey}+e`, {
      description: 'switch focus between main editor and notes',
      command: 'switch-surface',
    })
  },
}
