import { InsertNodeCommand } from 'substance'

class InsertOrnamentCommand extends InsertNodeCommand {
  getCommandState(params, context) {
    let sel = params.selection
    let newState = {
      disabled: true,
      active: false,
    }
    const { surface } = params

    if (surface && surface.props.textCommands) {
      if (
        !['full', this.name].some(e => {
          return surface.props.textCommands.indexOf(e) !== -1
        }) ||
        surface.props.editing === 'selection'
      ) {
        newState.disabled = true
        return newState
      }
    }

    if (sel && !sel.isNull() && !sel.isCustomSelection() && sel.containerId) {
      newState.disabled = false
    }
    newState.showInContext = this.showInContext(sel, params, context)
    return newState
  }

  createNodeData() {
    return {
      type: 'ornament',
    }
  }
}

export default InsertOrnamentCommand
