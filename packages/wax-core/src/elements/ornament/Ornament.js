import { BlockNode } from 'substance'

class Ornament extends BlockNode {}

Ornament.type = 'ornament'

export default Ornament
