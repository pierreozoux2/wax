import { Tool } from 'substance'
import Ornament from './Ornament'
import OrnamentComponent from './OrnamentComponent'
import InsertOrnamentCommand from './InsertOrnamentCommand'
import OrnamentHTMLConverter from './OrnamentHTMLConverter'

export default {
  name: 'ornament',
  configure: config => {
    config.addNode(Ornament)
    config.addComponent(Ornament.type, OrnamentComponent)
    config.addCommand('ornament', InsertOrnamentCommand, {
      nodeType: 'ornament',
      commandGroup: 'ornament',
    })
    config.addConverter('html', OrnamentHTMLConverter)
    config.addTool('ornament', Tool)
    config.addIcon('ornament', { fontawesome: 'fa-file-code-o' })
    config.addLabel('ornament', 'Add ornament')
  },
}
