import { debounce, isString, forEach, chunk } from 'lodash'
const DEFAULT_API_URL = 'http://localhost:8081/v2/check'

class SpellCheckManager {
  constructor(editorSession, options) {
    options = options || {}
    this.apiURL = options.apiURL || DEFAULT_API_URL
    let wait = options.wait || 750
    this.editorSession = editorSession

    // TODO: MarkersManager is basically a TextPropertyManager
    this.textPropertyManager = editorSession.markersManager
    this.markersManager = editorSession.markersManager

    this._schedule = {}
    this._scheduleCheck = debounce(this._runSpellCheck.bind(this), wait)

    editorSession.onFinalize('document', this._onDocumentChange, this)
  }

  dispose() {
    this.editorSession.off(this)
  }

  check(path) {
    this._runSpellCheck(String(path))
  }

  runGlobalCheck() {
    let paths = Object.keys(this.textPropertyManager._textProperties)
    paths.forEach(p => {
      this._runSpellCheck(p)
    })
  }

  _onDocumentChange(change, info) {
    console.log(this)
    const spellCheckStatus = this.editorSession.editor.state.spellCheck
    const textProperties = this.textPropertyManager._textProperties
    console.log(spellCheckStatus)
    if (!change.updated || !spellCheckStatus) return
    Object.keys(change.updated).forEach(pathStr => {
      if (textProperties[pathStr]) this._scheduleCheck(pathStr)
    })
  }

  _runSpellCheck(pathStr) {
    let path = pathStr.split(',')
    let text = this.editorSession.getDocument().get(path)
    let lang = this.editorSession.getLanguage()
    if (!text || !isString(text)) return
    const url = `https://languagetool.org/api/v2/check
?language=en-US&text=${text}`

    const params = {
      method: 'POST',
    }
    fetch(url, params)
      .then(data => {
        return data.json()
      })
      .then(res => {
        this._addSpellErrors(path, res)
      })
      .catch(error => {
        console.log(error)
      })
    return
  }

  /*
    Called when spell corrections have been returned.

    Removes all spell errors on the given path first.
  */
  _addSpellErrors(path, data) {
    console.log(path, data)
    const editorSession = this.editorSession
    const markersManager = editorSession.markersManager
    // NOTE: we have one set of markers for each text property
    // as we analyze each text block one by one
    const key = 'spell-error:' + path.join('.')

    const markers = data.matches.map(m => {
      return {
        type: 'spell-error',
        start: {
          path: path,
          offset: m.offset,
        },
        end: {
          offset: m.offset + m.length,
        },
        message: m.message,
        suggestions: m.replacements,
      }
    })

    markersManager.setMarkers(key, markers)

    setTimeout(() => {
      editorSession.startFlow()
    })
  }
}

export default SpellCheckManager
