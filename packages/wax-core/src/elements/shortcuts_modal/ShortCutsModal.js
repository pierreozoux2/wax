import { map } from 'lodash'
import { FontAwesomeIcon as Icon } from 'substance'
import Modal from '../modal/Modal'

class shortCutsModal extends Modal {
  constructor(...props) {
    super(...props)
    this.onEscapeCloseModal = this.onEscapeCloseModal.bind(this)
  }

  didMount() {
    document.addEventListener('keyup', this.onEscapeCloseModal, false)
  }

  render($$) {
    const KeyboardShortCuts = this.getKeyboardShortCuts()

    const el = $$('div').addClass('sc-modal sc-modal-short-cuts')

    const modalBody = $$('div').addClass('se-body-short-cuts')

    const closeButton = $$(Icon, { icon: 'fa-close' })
      .addClass('sc-close-modal')
      .on('click', this.closeModal)

    const modalHeader = $$('div')
      .addClass('sc-modal-header')
      .append('Keyboard shortcuts')
      .append(closeButton)

    const listItems = map(KeyboardShortCuts, (entry, name) => {
      const shortCutCombinations = entry.key.split('+')

      const arrayLength = shortCutCombinations.length
      const combinations = map(shortCutCombinations, (combination, index) => {
        const plusSign = index + 1 === arrayLength ? ' ' : '+'

        const comb = $$('span')
          .append(
            $$('span')
              .addClass('single-key')
              .append(combination),
          )
          .append(
            $$('span')
              .addClass('plus-sign')
              .append(plusSign),
          )

        return comb
      })

      const descripiton = entry.spec.description
        ? entry.spec.description
        : entry.spec.command

      const shortCut = $$('div')
        .addClass('shortcut-row')
        .append(
          $$('span')
            .addClass('shortcut-combination')
            .append(combinations),
        )
        .append(
          $$('span')
            .addClass('shortcut-description')
            .append(descripiton),
        )
      return shortCut
    })

    const modalContent = $$('div')
      .addClass('shortcuts-content-container')
      .append(
        $$('div')
          .addClass('shortcuts-content')
          .append(listItems),
      )

    modalBody.append(modalHeader, modalContent)
    el.append(modalBody)

    return el
  }

  getKeyboardShortCuts() {
    const { config } = this.context.editor.props.configurator
    return config.keyboardShortcuts
  }

  onEscapeCloseModal(event) {
    if ((event.keyCode || event.which) === 27) {
      this.context.editor.emit('toggleModal', 'shortCutsModal')
    }
  }

  closeModal() {
    const selection = this.context.editorSession.getSelection()
    this.context.editor.emit('toggleModal', 'shortCutsModal')
    document.removeEventListener('keyup', this.onEscapeCloseModal)
    this.context.editorSession.setSelection(selection)
  }
}

export default shortCutsModal
