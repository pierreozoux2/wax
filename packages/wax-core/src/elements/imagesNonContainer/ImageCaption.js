import { findIndex } from 'lodash'
import { Component } from 'substance'

class ImageCaption extends Component {
  render($$) {
    let el = $$('textarea')
      .attr('cols', '73')
      .attr('rows', '5')
      .attr('id', 'image-caption')

    el.on('keyup', this.saveCaption)
    el.attr('autofocus', true)
    el.attr('placeholder', 'insert a caption and press enter to save')
    return el
  }

  didMount() {
    const selection = this.context.editorSession.getSelection()
    const doc = this.context.editorSession.getDocument()
    const node = doc.get(selection.nodeId)
    document.getElementById('image-caption').value = node.caption
  }

  saveCaption(event) {
    // if not enter press return , otherwise save
    if (event.keyCode !== 13) return
    const editorSession = this.context.editorSession
    const selection = editorSession.getSelection()

    const imageId = selection.nodeId
    const newVal = this.el.val().replace(/(\r\n\t|\n|\r\t)/gm, '')

    editorSession.transaction(function(tx) {
      tx.set([imageId, 'caption'], newVal)
    })
    this.moveSelectionToNextBlock()
    editorSession.setSelection(null)
  }
  moveSelectionToNextBlock() {
    const editorSession = this.context.editorSession
    const selection = editorSession.getSelection()
    const containerId = selection.containerId
    const nodes = editorSession.document.data.nodes[containerId].nodes
    const index = findIndex(nodes, node => node === selection.nodeId)
    const nextnode = nodes[index + 1] ? nodes[index + 1] : undefined

    if (!nextnode) {
      editorSession.transaction(tx => {
        tx.break()
      })
    }

    setTimeout(() => {
      editorSession.transaction(tx => {
        tx.setSelection({
          type: 'property',
          containerId,
          surfaceId: containerId,
          path: [
            editorSession.document.data.nodes[containerId].nodes[index + 1],
            'content',
          ],
          startOffset: 0,
          endOffset: 0,
        })
      })
    })
  }
}

export default ImageCaption
