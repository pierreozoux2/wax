import { Tool } from 'substance'
import ImageCaption from './ImageCaption'

class EditCaptionTool extends Tool {
  render($$) {
    let el = $$('div')
      .addClass('caption-tool-container')
      .append($$(ImageCaption))
    return el
  }
}

export default EditCaptionTool
