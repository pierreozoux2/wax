import { TextBlockComponent } from 'substance'

class EpigraphProseComponent extends TextBlockComponent {
  render($$) {
    const el = super.render($$)
    return el.addClass('sc-epigraph-prose')
  }
}

export default EpigraphProseComponent
