export default {
  type: 'paragraph',
  tagName: 'p',

  matchElement: function(el, importer) {
    return el.is('p') && !el.hasClass('sc-continuous-paragraph')
  },

  import: (el, node, converter) => {
    node.content = converter.annotatedText(el, [node.id, 'content'])
  },

  export: (node, el, converter) => {
    el.append(converter.annotatedText([node.id, 'content']))
  },
}
