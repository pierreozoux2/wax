import { AnnotationTool } from 'substance'

class SourceNoteTool extends AnnotationTool {}

SourceNoteTool.type = 'source-note'

export default SourceNoteTool
