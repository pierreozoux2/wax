import { isMatch } from 'lodash'
import { SwitchTextTypeCommand } from 'substance'

class ChapterTitleCommand extends SwitchTextTypeCommand {
  getType() {
    return this.config.spec.type
  }

  getCommandState(params) {
    let doc = params.editorSession.getDocument()
    let sel = params.selection
    let isBlurred = params.editorSession.isBlurred()

    if (!params.surface) return
    const provider = params.surface.context.chapterTitleProvider
    const title = provider.getTitle()

    let commandState = {
      disabled: false,
    }

    if (title) {
      commandState = {
        disabled: true,
      }

      return commandState
    }

    if (params.surface && !params.surface.context.editor.props.mode.styling) {
      commandState.disabled = true
      return commandState
    }

    if (params.surface && params.surface.containerId !== 'main') {
      commandState.disabled = true
      return commandState
    }

    if (sel.isPropertySelection() && !isBlurred) {
      let path = sel.getPath()
      let node = doc.get(path[0])
      if (node && node.isText() && node.isBlock()) {
        commandState.active = isMatch(node, this.config.spec)
        // When cursor is at beginning of a text block we signal
        // that we want the tool to appear contextually (e.g. in an overlay)
        commandState.showInContext =
          sel.start.offset === 0 && sel.end.offset === 0
      } else {
        commandState.disabled = true
      }
    } else {
      // TODO: Allow Container Selections too, to switch multiple paragraphs
      commandState.disabled = true
    }

    return commandState
  }

  /**
    Perform a switchTextType transformation based on the current selection
  */
  execute(params) {
    let surface = params.surface
    let editorSession = params.editorSession
    if (!surface) {
      console.warn('No focused surface. Stopping command execution.')
      return
    }
    editorSession.transaction(tx => {
      return tx.switchTextType(this.config.spec)
    })
  }

  isSwitchTypeCommand() {
    return true
  }
}

export default ChapterTitleCommand
