import { TextBlock } from 'substance'

class ChapterTitle extends TextBlock {}

ChapterTitle.type = 'chapter-title'

export default ChapterTitle
