import PartNumber from './PartNumber'
import PartNumberComponent from './PartNumberComponent'
import PartNumberHTMLConverter from './PartNumberHTMLConverter'
import WaxSwitchTextTypeCommand from '../../../commands/WaxSwitchTextTypeCommand'

export default {
  name: 'part-number',
  configure: config => {
    config.addNode(PartNumber)
    config.addComponent(PartNumber.type, PartNumberComponent)
    config.addConverter('html', PartNumberHTMLConverter)
    config.addCommand('part-number', WaxSwitchTextTypeCommand, {
      spec: { type: 'part-number' },
      commandGroup: 'text-display-part',
    })

    config.addLabel('part-number', {
      en: 'Part Number',
    })
  },
  PartNumber: PartNumber,
  PartNumberComponent: PartNumberComponent,
  PartNumberHTMLConverter: PartNumberHTMLConverter,
}
