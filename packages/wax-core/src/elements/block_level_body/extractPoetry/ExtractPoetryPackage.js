import ExtractPoetry from './ExtractPoetry'
import ExtractPoetryComponent from './ExtractPoetryComponent'
import ExtractPoetryHTMLConverter from './ExtractPoetryHTMLConverter'
import WaxSwitchTextTypeCommand from '../../../commands/WaxSwitchTextTypeCommand'

export default {
  name: 'extract-poetry',
  configure: config => {
    config.addNode(ExtractPoetry)

    config.addCommand('extract-poetry-part', WaxSwitchTextTypeCommand, {
      spec: { type: 'extract-poetry' },
      commandGroup: 'text-types-part',
    })
    config.addCommand('extract-poetry-chapter', WaxSwitchTextTypeCommand, {
      spec: { type: 'extract-poetry' },
      commandGroup: 'text-types-chapter',
    })
    config.addComponent(ExtractPoetry.type, ExtractPoetryComponent)
    config.addConverter('html', ExtractPoetryHTMLConverter)

    config.addLabel('extract-poetry-part', {
      en: 'Extract: Poetry',
    })
    config.addLabel('extract-poetry-chapter', {
      en: 'Extract: Poetry',
    })
  },
}
