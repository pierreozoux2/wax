import Author from './Author'
import AuthorComponent from './AuthorComponent'
import AuthorHTMLConverter from './AuthorHTMLConverter'
import WaxSwitchTextTypeCommand from '../../../commands/WaxSwitchTextTypeCommand'

export default {
  name: 'author',
  configure: config => {
    config.addNode(Author)
    config.addComponent(Author.type, AuthorComponent)
    config.addConverter('html', AuthorHTMLConverter)
    config.addCommand('author', WaxSwitchTextTypeCommand, {
      spec: { type: 'author' },
      commandGroup: 'text-display-chapter',
    })

    config.addCommand('front-author', WaxSwitchTextTypeCommand, {
      spec: { type: 'author' },
      commandGroup: 'front-matter-a',
    })
    config.addLabel('author', {
      en: 'Author',
    })

    config.addLabel('front-author', {
      en: 'Author',
    })
  },
  Author: Author,
  AuthorComponent: AuthorComponent,
  AuthorHTMLConverter: AuthorHTMLConverter,
}
