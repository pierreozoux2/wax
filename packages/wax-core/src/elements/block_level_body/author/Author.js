import { TextBlock } from 'substance'

class Author extends TextBlock {}

Author.type = 'author'

export default Author
