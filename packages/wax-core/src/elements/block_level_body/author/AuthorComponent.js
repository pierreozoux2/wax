import { TextBlockComponent } from 'substance'

class AuthorComponent extends TextBlockComponent {
  render($$) {
    let el = super.render($$)
    return el.addClass('sc-author')
  }
}

export default AuthorComponent
