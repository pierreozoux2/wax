import Heading from './Heading'
import HeadingComponent from './HeadingComponent'
import HeadingMacro from './HeadingMacro'
import HeadingHTMLConverter from './HeadingHTMLConverter'
import WaxSwitchTextTypeCommand from '../../../commands/WaxSwitchTextTypeCommand'

export default {
  name: 'heading',
  configure: function(config) {
    config.addNode(Heading)
    config.addComponent(Heading.type, HeadingComponent)
    config.addConverter('html', HeadingHTMLConverter)

    config.addCommand('heading1', WaxSwitchTextTypeCommand, {
      spec: { type: 'heading', level: 1 },
      commandGroup: 'text-display-headers',
    })
    config.addCommand('heading2', WaxSwitchTextTypeCommand, {
      spec: { type: 'heading', level: 2 },
      commandGroup: 'text-display-headers',
    })
    config.addCommand('heading3', WaxSwitchTextTypeCommand, {
      spec: { type: 'heading', level: 3 },
      commandGroup: 'text-display-headers',
    })

    config.addLabel('heading1', {
      en: 'Heading 1',
    })
    config.addLabel('heading2', {
      en: 'Heading 2',
    })
    config.addLabel('heading3', {
      en: 'Heading 3',
    })

    // config.addIcon('heading1', { fontawesome: 'fa-header' })
    // config.addIcon('heading2', { fontawesome: 'fa-header' })
    // config.addIcon('heading3', { fontawesome: 'fa-header' })
  },
  Heading,
  HeadingComponent,
  HeadingHTMLConverter,
  HeadingMacro,
}
