import { TextBlock } from 'substance'

class ParagraphContd extends TextBlock {}

ParagraphContd.type = 'paragraph-contd'

export default ParagraphContd
