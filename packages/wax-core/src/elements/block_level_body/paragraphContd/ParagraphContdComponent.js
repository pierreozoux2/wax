import { TextBlockComponent } from 'substance'

class ParagraphContdComponent extends TextBlockComponent {
  render($$) {
    const el = super.render($$)
    el.addClass('sc-continuous-paragraph')

    this.props.node.class = 'sc-continuous-paragraph'
    return el
  }

  didMount() {
    const { editorSession } = this.context
    editorSession.onRender('document', this.updateClassNames, this)
  }

  updateClassNames() {
    if (this.textContent === '') {
      this.addClass('empty-paragraph')
    } else {
      this.removeClass('empty-paragraph')
    }
  }
}

export default ParagraphContdComponent
