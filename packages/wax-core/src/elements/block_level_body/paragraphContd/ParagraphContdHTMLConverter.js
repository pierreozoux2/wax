export default {
  type: 'paragraph-contd',
  tagName: 'p',

  matchElement: function(el, importer) {
    return el.is('p') && el.hasClass('sc-continuous-paragraph')
  },

  import: (el, node, converter) => {
    node.content = converter.annotatedText(el, [node.id, 'content'])
    node.class = el.attr('class')
  },

  export: (node, el, converter) => {
    el.setAttribute('class', node.class)
    el.append(converter.annotatedText([node.id, 'content']))
  },
}
