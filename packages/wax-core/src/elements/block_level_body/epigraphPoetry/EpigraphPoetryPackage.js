import EpigraphPoetry from './EpigraphPoetry'
import EpigraphPoetryComponent from './EpigraphPoetryComponent'
import EpigraphPoetryHTMLConverter from './EpigraphPoetryHTMLConverter'
import WaxSwitchTextTypeCommand from '../../../commands/WaxSwitchTextTypeCommand'

export default {
  name: 'epigraph-poetry',
  configure: config => {
    config.addNode(EpigraphPoetry)
    config.addCommand('epigraph-poetry-part', WaxSwitchTextTypeCommand, {
      spec: { type: 'epigraph-poetry' },
      commandGroup: 'text-display-part',
    })
    config.addCommand('epigraph-poetry-chapter', WaxSwitchTextTypeCommand, {
      spec: { type: 'epigraph-poetry' },
      commandGroup: 'text-display-chapter',
    })
    config.addCommand('epigraph-poetry-front', WaxSwitchTextTypeCommand, {
      spec: { type: 'epigraph-poetry' },
      commandGroup: 'front-matter-b',
    })
    config.addComponent(EpigraphPoetry.type, EpigraphPoetryComponent)
    config.addConverter('html', EpigraphPoetryHTMLConverter)

    config.addLabel('epigraph-poetry-part', {
      en: 'Epigraph: Poetry',
    })
    config.addLabel('epigraph-poetry-chapter', {
      en: 'Epigraph: Poetry',
    })
    config.addLabel('epigraph-poetry-front', {
      en: 'Epigraph: Poetry',
    })
  },
}
