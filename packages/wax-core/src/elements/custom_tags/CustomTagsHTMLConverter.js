export default {
  type: 'custom-tags',
  tagName: 'custom-inline',

  import: (element, node) => {
    node.class = element.attr('class')
  },

  export: (node, element) => {
    element.setAttribute('class', node.class)
  },
}
