import { PropertyAnnotation } from 'substance'

class CustomTags extends PropertyAnnotation {}

CustomTags.schema = {
  type: 'custom-tags',
  class: {
    type: 'string',
    default: '',
  },
}

export default CustomTags
