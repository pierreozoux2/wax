import CustomTags from './CustomTags'
import CustomTagsTool from './CustomTagsTool'
import WaxAnnotationCommand from '../../commands/WaxAnnotationCommand'
import CustomTagsHTMLConverter from './CustomTagsHTMLConverter'

export default {
  name: 'custom-tags',
  configure: function(config) {
    config.addNode(CustomTags)
    config.addCommand(CustomTags.type, WaxAnnotationCommand, {
      nodeType: 'custom-tags',
      commandGroup: 'custom-tags',
    })
    config.addTool('custom-tags', CustomTagsTool)
    config.addConverter('html', CustomTagsHTMLConverter)
  },
}
