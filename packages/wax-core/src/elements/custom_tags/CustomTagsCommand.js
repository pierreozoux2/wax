import { Command } from 'substance'

class CustomTagsCommand extends Command {
  getCommandState(params) {
    const sel = params.selection
    const surface = params.surface
    const newState = {
      active: false,
      disabled: true,
    }
    if (!sel.isCollapsed() && !sel.isContainerSelection()) {
      newState.disabled = false
    }
    return newState
  }
}

export default CustomTagsCommand
