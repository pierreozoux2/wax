/* eslint react/prop-types: 0 */
import { Component } from 'substance'

class ReplaceField extends Component {
  render($$) {
    const searchInput = $$('div')
      .addClass('find-and-replace-search-container')
      .append(
        $$('div')
          .addClass('search-text')
          .append('Replace with :'),
      )
      .append(
        $$('input')
          .addClass('search-input')
          .attr('id', 'replace-text')
          .attr('name', 'search')
          .attr('value', '')
          .attr('autocomplete', 'off')
          .on('keydown', this.props.enterSearch)
          .ref('search-diacrtics'),
      )
    return searchInput
  }
}

export default ReplaceField
