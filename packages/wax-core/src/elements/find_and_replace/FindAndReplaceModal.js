import { each, eachRight } from 'lodash'
import { documentHelpers, FontAwesomeIcon as Icon } from 'substance'

import Modal from '../modal/Modal'
import SearchField from './SearchField'
import ReplaceField from './ReplaceField'

class FindAndReplaceModal extends Modal {
  constructor(...props) {
    super(...props)
    this.findInText = this.findInText.bind(this)
    this.enterSearch = this.enterSearch.bind(this)
    this.searchStringLength = 0
  }

  render($$) {
    const el = $$('div').addClass('sc-modal-find-and-replace')

    const findIcon = $$(Icon, { icon: 'fa-search' }).addClass('sc-find-icon')

    const closeButton = $$(Icon, { icon: 'fa-close' })
      .addClass('sc-close-modal')
      .on('click', this.closeModal)

    const findNextButton = $$('button')
      .append('Find Next')
      .addClass('sc-replace-button')
      .on('click', this.findNext)

    const findPreviousButton = $$('button')
      .append('Find Previous')
      .addClass('sc-replace-button')
      .on('click', this.findPrevious)

    const replaceButton = $$('button')
      .append('Replace')
      .addClass('sc-replace-button')
      .on('click', this.replace)

    const replaceAllButton = $$('button')
      .append('Replace All')
      .addClass('sc-replace-button')
      .on('click', this.replaceAll)

    const buttonContainer = $$('div')
      .addClass('find-and-replace-button-container')
      .append(replaceAllButton)
      .append(replaceButton)
      .append(findPreviousButton)
      .append(findNextButton)

    const searchField = $$(SearchField, {
      onKeyUp: this.findInText,
      onFocus: this.findInText,
      enterSearch: this.enterSearch,
    }).ref('search-field')

    const replaceField = $$(ReplaceField, {
      enterSearch: this.enterSearch,
    }).ref('replace-field')

    const modalHeader = $$('div')
      .addClass('sc-modal-header-find-and-replace')
      .append(
        $$('span')
          .addClass('sc-header-text')
          .append('Search And Replace'),
      )
      .append(findIcon)
      .append(closeButton)

    const totalMatches =
      this.state.matches.length === 0 ? 0 : this.state.matches.length

    const totalMatchesEl = $$('span')
      .addClass('total-matches')
      .append(`Matches found: ${totalMatches}`)

    const modalBody = $$('div')
      .addClass('sc-modal-body')
      .append(searchField)
      .append(replaceField)
      .append(buttonContainer)
      .append(totalMatchesEl)

    el.append(
      $$('div')
        .addClass('se-body-find-and-replace')
        .append(modalHeader)
        .append(modalBody),
    )
    return el
  }

  findInText() {
    const provider = this.getProvider()
    const element = document.getElementById('search-text')
    const pattern = element.value
    let textProperties = []
    const editorSession = this.getEditorSession()
    const doc = editorSession.getDocument()
    const rootElement = this.context.editor.el

    textProperties = rootElement.findAll('.sc-text-property').map(property => {
      return {
        path: property._comp.props.path,
        containerId: property._comp.parent.parent.containerId || 'main',
      }
    })

    let matches = []
    if (pattern) {
      textProperties.forEach(textProperty => {
        const found = provider.findInTextProperty({
          path: textProperty.path,
          containerId: textProperty.containerId,
          findString: pattern,
          editorSession,
        })
        matches = matches.concat(found)
      })
    }

    matches = provider.excludeTrackChanges(matches, doc)
    if (
      matches.length === this.state.matches.length &&
      this.searchStringLength === pattern.length
    )
      return
    this.searchStringLength = pattern.length
    this.extendState({ matches })
    provider.highLightMatches(editorSession, matches, doc)
    element.focus()
  }

  findNext() {
    const provider = this.getProvider()
    return provider.findNext()
  }

  findPrevious() {
    const provider = this.getProvider()
    return provider.findPrevious()
  }

  enterSearch(event) {
    if (event && event.keyCode === 13) {
      event.preventDefault()
      this.findNext()
    }
  }

  replace() {
    const provider = this.getProvider()
    const editorSession = this.getEditorSession()
    const selection = editorSession.getSelection()
    const doc = editorSession.getDocument()
    const matches = this.state.matches
    const trackChanges = this.context.editor.props.trackChanges

    const findAndReplace = documentHelpers.getPropertyAnnotationsForSelection(
      doc,
      selection,
      { type: 'find-and-replace' },
    )

    if (findAndReplace.length === 0 && matches.length > 0) {
      provider.createSelection(editorSession, matches[0])
      return
    }

    const replaceElement = document.getElementById('replace-text')
    const replaceValue = replaceElement.value

    provider.clearMatches(doc, editorSession)

    if (findAndReplace.length === 1) {
      if (trackChanges) {
        const match = matches.filter(m => {
          return (
            findAndReplace[0].start.offset === m.start.offset &&
            findAndReplace[0].end.offset === m.end.offset &&
            findAndReplace[0].path[0] === m.path[0]
          )
        })

        const { user } = this.context.editor.props
        provider.creteTrackDeletionAndInsert(
          match,
          replaceValue,
          user,
          editorSession,
        )
      } else {
        editorSession.transaction(tx => {
          tx.setSelection(findAndReplace[0].getSelection())
          tx.insertText(replaceValue)
        })
      }
      this.findInText()
    }
  }

  replaceAll() {
    const provider = this.getProvider()
    const editorSession = this.getEditorSession()
    const matches = this.state.matches
    const doc = editorSession.getDocument()
    const replaceElement = document.getElementById('replace-text')
    const replaceValue = replaceElement.value
    const trackChanges = this.context.editor.props.trackChanges

    provider.clearMatches(doc, editorSession)

    if (trackChanges) {
      const user = this.context.editor.props.user
      provider.creteTrackDeletionAndInsert(
        matches,
        replaceValue,
        user,
        editorSession,
      )
    } else {
      editorSession.transaction(tx => {
        eachRight(matches, match => {
          tx.setSelection({
            type: 'property',
            containerId: match.containerId,
            surfaceId: match.surfaceId,
            path: match.path,
            startOffset: match.start.offset,
            endOffset: match.end.offset,
          })
          tx.insertText(replaceValue)
        })
      })
    }
    this.findInText()
  }

  getEditorSession() {
    return this.context.editorSession
  }

  closeModal() {
    const provider = this.getProvider()
    const editorSession = this.getEditorSession()
    const doc = editorSession.getDocument()
    provider.clearMatches(doc, editorSession)
    this.context.editor.emit('toggleModal', 'findAndReplaceModal')
  }

  getInitialState() {
    return {
      matches: '',
    }
  }

  getProvider() {
    return this.context.findAndReplaceProvider
  }
}

export default FindAndReplaceModal
