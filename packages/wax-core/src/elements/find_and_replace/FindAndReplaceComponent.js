import { AnnotationComponent } from 'substance'

class FindAndReplaceComponent extends AnnotationComponent {
  render($$) {
    const el = $$('span')
      .attr('data-id', this.props.node.id)
      .addClass(this.getClassNames())

    el.append(this.props.children)
    // el.on('click', this.focus)

    return el
  }
}

export default FindAndReplaceComponent
