import { TextBlockComponent } from 'substance'

class DedicationComponent extends TextBlockComponent {
  render($$) {
    let el = super.render($$)
    return el.addClass('sc-dedication')
  }
}

export default DedicationComponent
