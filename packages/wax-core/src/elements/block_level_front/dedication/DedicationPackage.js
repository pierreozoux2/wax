import Dedication from './Dedication'
import DedicationComponent from './DedicationComponent'
import DedicationHTMLConverter from './DedicationHTMLConverter'
import WaxSwitchTextTypeCommand from '../../../commands/WaxSwitchTextTypeCommand'

export default {
  name: 'dedication',
  configure: config => {
    config.addNode(Dedication)
    config.addComponent(Dedication.type, DedicationComponent)
    config.addConverter('html', DedicationHTMLConverter)

    config.addCommand('dedication', WaxSwitchTextTypeCommand, {
      spec: { type: 'dedication' },
      commandGroup: 'front-matter-b',
    })

    config.addLabel('dedication', {
      en: 'Dedication',
    })
  },
  Dedication: Dedication,
  DedicationComponent: DedicationComponent,
  DedicationHTMLConverter: DedicationHTMLConverter,
}
