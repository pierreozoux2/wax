import SeriesEditor from './SeriesEditor'
import SeriesEditorComponent from './SeriesEditorComponent'
import SeriesEditorHTMLConverter from './SeriesEditorHTMLConverter'
import WaxSwitchTextTypeCommand from '../../../commands/WaxSwitchTextTypeCommand'

export default {
  name: 'series-editor',
  configure: config => {
    config.addNode(SeriesEditor)
    config.addComponent(SeriesEditor.type, SeriesEditorComponent)
    config.addConverter('html', SeriesEditorHTMLConverter)

    config.addCommand('series-editor', WaxSwitchTextTypeCommand, {
      spec: { type: 'series-editor' },
      commandGroup: 'front-matter-a',
    })

    config.addLabel('series-editor', {
      en: 'Series Editor',
    })
  },
  SeriesEditor: SeriesEditor,
  SeriesEditorComponent: SeriesEditorComponent,
  SeriesEditorHTMLConverter: SeriesEditorHTMLConverter,
}
