import { TextBlock } from 'substance'

class HalfTitle extends TextBlock {}

HalfTitle.type = 'half-title'

export default HalfTitle
