import HalfTitle from './HalfTitle'
import HalfTitleComponent from './HalfTitleComponent'
import HalfTitleHTMLConverter from './HalfTitleHTMLConverter'
import WaxSwitchTextTypeCommand from '../../../commands/WaxSwitchTextTypeCommand'

export default {
  name: 'half-title',
  configure: config => {
    config.addNode(HalfTitle)
    config.addComponent(HalfTitle.type, HalfTitleComponent)
    config.addConverter('html', HalfTitleHTMLConverter)
    config.addCommand('half-title', WaxSwitchTextTypeCommand, {
      spec: { type: 'half-title' },
      commandGroup: 'front-matter-a',
    })

    config.addLabel('half-title', {
      en: 'Half Title',
    })
  },
  HalfTitle: HalfTitle,
  HalfTitleComponent: HalfTitleComponent,
  HalfTitleHTMLConverter: HalfTitleHTMLConverter,
}
