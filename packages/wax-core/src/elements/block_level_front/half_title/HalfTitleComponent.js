import { TextBlockComponent } from 'substance'

class HalfTitleComponent extends TextBlockComponent {
  render($$) {
    let el = super.render($$)
    return el.addClass('sc-half-title')
  }
}

export default HalfTitleComponent
