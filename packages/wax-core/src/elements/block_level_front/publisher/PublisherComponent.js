import { TextBlockComponent } from 'substance'

class PublisherComponent extends TextBlockComponent {
  render($$) {
    let el = super.render($$)
    return el.addClass('sc-publisher')
  }
}

export default PublisherComponent
