import Publisher from './Publisher'
import PublisherComponent from './PublisherComponent'
import PublisherHTMLConverter from './PublisherHTMLConverter'
import WaxSwitchTextTypeCommand from '../../../commands/WaxSwitchTextTypeCommand'

export default {
  name: 'publisher',
  configure: config => {
    config.addNode(Publisher)
    config.addComponent(Publisher.type, PublisherComponent)
    config.addConverter('html', PublisherHTMLConverter)

    config.addCommand('publisher', WaxSwitchTextTypeCommand, {
      spec: { type: 'publisher' },
      commandGroup: 'front-matter-a',
    })

    config.addLabel('publisher', {
      en: 'Publisher',
    })
  },
  Publisher: Publisher,
  PublisherComponent: PublisherComponent,
  PublisherHTMLConverter: PublisherHTMLConverter,
}
