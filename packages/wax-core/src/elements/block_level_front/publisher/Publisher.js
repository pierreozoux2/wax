import { TextBlock } from 'substance'

class Publisher extends TextBlock {}

Publisher.type = 'publisher'

export default Publisher
