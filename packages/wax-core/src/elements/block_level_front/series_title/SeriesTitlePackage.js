import SeriesTitle from './SeriesTitle'
import SeriesTitleComponent from './SeriesTitleComponent'
import SeriesTitleHTMLConverter from './SeriesTitleHTMLConverter'
import WaxSwitchTextTypeCommand from '../../../commands/WaxSwitchTextTypeCommand'

export default {
  name: 'series-title',
  configure: config => {
    config.addNode(SeriesTitle)
    config.addComponent(SeriesTitle.type, SeriesTitleComponent)
    config.addConverter('html', SeriesTitleHTMLConverter)

    config.addCommand('series-title', WaxSwitchTextTypeCommand, {
      spec: { type: 'series-title' },
      commandGroup: 'front-matter-a',
    })

    config.addLabel('series-title', {
      en: 'Series Title',
    })
  },
  SeriesTitle: SeriesTitle,
  SeriesTitleComponent: SeriesTitleComponent,
  SeriesTitleHTMLConverter: SeriesTitleHTMLConverter,
}
