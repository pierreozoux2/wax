import { TextBlock } from 'substance'

class SeriesTitle extends TextBlock {}

SeriesTitle.type = 'series-title'

export default SeriesTitle
