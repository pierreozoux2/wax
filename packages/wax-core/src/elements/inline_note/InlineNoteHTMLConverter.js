export default {
  type: 'inline-note',
  tagName: 'inline-note',

  import: (element, node, converter) => {
    node.reference = element.attr('reference')
    node.number = element.attr('number')
  },

  export: (node, element, converter) => {
    element.setAttribute('reference', node.reference)
    element.setAttribute('number', node.number)
  },
}
