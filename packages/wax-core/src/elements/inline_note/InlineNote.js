import { InlineNode } from 'substance'

class InlineNote extends InlineNode {}

InlineNote.schema = {
  type: 'inline-note',
  reference: {
    type: String,
    default: '',
  },
  number: {
    type: String,
    default: '',
  },
}

export default InlineNote
