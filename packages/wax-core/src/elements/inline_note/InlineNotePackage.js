import InlineNote from './InlineNote'
import InlineNoteCommand from './InlineNoteCommand'
import InlineNoteComponent from './InlineNoteComponent'
import InlineNoteHTMLConverter from './InlineNoteHTMLConverter'
import InlineNoteTool from './InlineNoteTool'

export default {
  name: 'inline-note',
  configure: function(config) {
    config.addNode(InlineNote)

    config.addComponent(InlineNote.type, InlineNoteComponent)
    config.addConverter('html', InlineNoteHTMLConverter)
    config.addCommand(InlineNote.type, InlineNoteCommand, {
      spec: { type: 'inline-note' },
      commandGroup: 'inline-note',
    })

    config.addTool('inline-note', InlineNoteTool)
    config.addIcon('inline-note', { fontawesome: 'fa-bookmark' })
    config.addLabel('inline-note', {
      en: 'Inline  note',
    })
  },
}
