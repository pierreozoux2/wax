import { InsertInlineNodeCommand } from 'substance'

class InlineNoteCommand extends InsertInlineNodeCommand {
  getCommandState(params) {
    const surface = params.surface
    const newState = {
      disabled: false,
      active: false,
    }

    if (surface && surface.name === 'main') {
      newState.disabled = true
    }

    return newState
  }

  execute(params) {
    const editor = params.surface.context.editor
    const editorSession = params.editorSession

    editorSession.transaction(tx => {
      return tx.insertInlineNode({
        type: 'inline-note',
      })
    })
  }
}

InlineNoteCommand.type = 'inline-note'

export default InlineNoteCommand
