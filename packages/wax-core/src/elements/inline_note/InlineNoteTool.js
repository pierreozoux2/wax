import { Tool } from 'substance'

class InlineNoteTool extends Tool {
  renderButton($$) {
    const el = super.renderButton($$)
    return el
  }
}
InlineNoteTool.type = 'inline-note'

export default InlineNoteTool
