import { Command } from 'substance'

const ELLIPSIS = '\u2026'
const THINSPACE = '\u2009'
class InsertEllipsesCommand extends Command {
  getCommandState(params, context) {
    return {
      disabled: false,
    }
  }

  execute(params, context) {
    // eslint-disable-line
    let editorSession = params.editorSession
    let sel = editorSession.getSelection()
    let doc = editorSession.getDocument()
    if (sel.isPropertySelection()) {
      let nodeId = sel.start.getNodeId()
      let node = doc.get(nodeId)
      if (node.isText()) {
        const { trackChanges } = params.surface.props
        let text = node.getText()
        let offset = sel.start.offset
        const oneSpaceNextLetter = text.slice(offset + 1, offset + 2)
        const noSpaceNextLetter = text.slice(offset, offset + 1)
        // const previoustLetter = text.slice(offset, offset + 1)
        let mark
        let capital = false
        if (
          oneSpaceNextLetter.match(/^[A-Z]*$/) ||
          noSpaceNextLetter.match(/^[A-Z]*$/)
        ) {
          capital = true
        }
        mark = ELLIPSIS
        if (trackChanges) {
          this.createTrackEllipses(text, offset, params, sel, mark, capital)
          return
        }
        if (text.slice(offset - 2, offset) === '..' && !capital) {
          editorSession.transaction(tx => {
            const forDeletion = tx.setSelection({
              type: 'property',
              containerId: sel.containerId,
              surfaceId: sel.surfaceId,
              path: sel.path,
              startOffset: sel.start.offset - 2,
              endOffset: sel.end.offset,
            })
            tx.deleteSelection(forDeletion)
            tx.insertText(THINSPACE)
            tx.insertText(mark)
          })
        } else if (text.slice(offset - 3, offset) === '...' && capital) {
          editorSession.transaction(tx => {
            const forDeletion = tx.setSelection({
              type: 'property',
              containerId: sel.containerId,
              surfaceId: sel.surfaceId,
              path: sel.path,
              startOffset: sel.start.offset - 2,
              endOffset: sel.end.offset,
            })
            tx.deleteSelection(forDeletion)
            tx.insertText(THINSPACE)
            tx.insertText(mark)
            capital = false
          })
        } else {
          editorSession.transaction(tx => {
            tx.insertText('.')
          })
          return
        }
        return true
      }
    }
    return false
  }

  createTrackEllipses(text, offset, params, sel, mark, capital) {
    const { surface, editorSession } = params
    const { user } = surface.context.editor.props
    const { containerId } = surface

    let surfaceId = containerId
    if (containerId.includes('caption')) {
      surfaceId = `main/${containerId.substring(8)}/${containerId}`
    }

    if (text.slice(offset - 2, offset) === '..' && !capital) {
      editorSession.transaction(tx => {
        const forDeletion = tx.setSelection({
          type: 'property',
          containerId: sel.containerId,
          surfaceId: sel.surfaceId,
          path: sel.path,
          startOffset: sel.start.offset - 2,
          endOffset: sel.end.offset,
        })
        tx.deleteSelection(forDeletion)
        tx.insertText(THINSPACE)
        tx.insertText(mark)
        tx.create({
          status: 'add',
          type: 'track-change',
          containerId,
          surfaceId,
          path: sel.path,
          startOffset: sel.start.offset - 1,
          endOffset: sel.start.offset,
          user: {
            id: user.id,
            username: user.username,
            color: user.color,
          },
        })
      })
    } else if (text.slice(offset - 3, offset) === '...' && capital) {
      editorSession.transaction(tx => {
        const forDeletion = tx.setSelection({
          type: 'property',
          containerId: sel.containerId,
          surfaceId: sel.surfaceId,
          path: sel.path,
          startOffset: sel.start.offset - 2,
          endOffset: sel.end.offset,
        })
        tx.deleteSelection(forDeletion)
        tx.insertText(THINSPACE)
        tx.insertText(mark)
        tx.create({
          status: 'add',
          type: 'track-change',
          containerId,
          surfaceId,
          path: sel.path,
          startOffset: sel.start.offset - 1,
          endOffset: sel.start.offset,
          user: {
            id: user.id,
            username: user.username,
            color: user.color,
          },
        })
        capital = false
      })
    } else {
      editorSession.transaction(tx => {
        tx.insertText('.')
        tx.create({
          status: 'add',
          type: 'track-change',
          containerId,
          surfaceId,
          path: sel.path,
          startOffset: sel.start.offset,
          endOffset: sel.end.offset + 1,
          user: {
            id: user.id,
            username: user.username,
            color: user.color,
          },
        })
      })
      return
    }
  }
}

export default InsertEllipsesCommand
