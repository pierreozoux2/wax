import { Container } from 'substance'

class MainNode extends Container {}

MainNode.schema = {
  type: 'container',
  id: 'main',
}

export default MainNode
