import { each, keys, values, includes } from 'lodash'
import {
  ContainerEditor as SubstanceContainerEditor,
  keys as keyboardKeys,
  uuid,
  documentHelpers,
} from 'substance'

class ContainerEditor extends SubstanceContainerEditor {
  constructor(...props) {
    super(...props)
    this.context.editorSession.onUpdate('document', this.checkChange, this)
    this.focus = this.focus.bind(this)
    this.spellchecker = false
  }

  render($$) {
    const el = super.render($$)
    el.addEventListener('focus', this.focus, this)
    return el
  }

  didMount() {
    super.didMount()

    if (this.isEmpty() && this.props.editing === 'full') {
      this.createText()
    }

    if (this.props.editing === 'full') this.setFocusOnStartOfText()

    // Check is this is working properly as the isReadOnlyMode is unstable
    if (this.isReadOnlyMode()) {
      this.addTargetToLinks()
    }
  }

  focus() {
    return false
  }

  // create an empty paragraph with an empty node
  // then select it for cursor focus

  createText() {
    const doc = this.context.editorSession.document
    const container = doc.get(this.props.containerId)
    const textType = doc.getSchema().getDefaultTextType()

    const node = this.context.editorSession.document.create({
      id: uuid(textType),
      type: textType,
      content: '',
    })

    container.show(node.id)
    this.rerender()
  }

  setFocusOnStartOfText() {
    const doc = this.context.editorSession.getDocument()
    const nodes = doc.getNodes()
    const mainNodes = nodes[this.props.containerId].nodes
    const firstNode = doc.get(mainNodes[0])
    if (typeof firstNode === 'undefined' || firstNode.type === 'script') return
    if (this.props.containerId === 'main') {
      const nodeId =
        firstNode.type === 'list' ? firstNode.items[0] : firstNode.id

      this.context.editorSession.transaction(tx => {
        if (firstNode.type === 'image') {
          tx.setSelection({
            type: 'node',
            containerId: this.props.containerId,
            nodeId: firstNode.id,
            startOffset: 0,
            endOffset: 1,
          })
        } else {
          this.setSelection(tx, nodeId)
        }
      })
    }
  }

  // TODO -- better name to avoid confusion with editorsession.setSelection
  setSelection(tx, startPathId, startOffset = 0, endOffset = 0) {
    tx.setSelection({
      type: 'property',
      containerId: this.props.containerId,
      surfaceId: this.props.containerId,
      path: [startPathId, 'content'],
      startOffset,
      endOffset,
    })
  }

  isReadOnlyMode() {
    return !this.isEditable() && this.isSelectable()
  }

  addTargetToLinks() {
    const allLinks = this.el.findAll('a')
    each(allLinks, link => link.attr('target', '_blank'))
  }

  getEditor() {
    return this.context.editor
  }

  _handleTabKey(event) {
    const { editorSession } = this.context
    const selection = editorSession.getSelection()
    const doc = editorSession.getDocument()
    const node = doc.get(selection.path[0])
    if (node.isListItem() && node.id === node.parent.items[0]) {
      return event.preventDefault()
    } else {
      super._handleTabKey(event)
    }
  }

  // Track Changes Start (should be removed from ContainerEditor)

  onTextInput(event) {
    const selection = this.context.editorSession.getSelection()
    if (!this.props.trackChanges) return super.onTextInput(event)

    // Text Input Bindings
    var hook = this.editorSession.keyboardManager.textinputBindings[event.data]
    if (hook) {
      setTimeout(() => {
        var params = this.editorSession.keyboardManager._getParams()
        return hook(params, this.context)
      })
      return
    }
    if (selection.type === 'node') super._handleDeleteKey(event)

    return this.handleTracking({
      event,
      status: 'add',
      surfaceEvent: 'input',
    })
  }

  onTextInputShim(event) {
    const selection = this.context.editorSession.getSelection()
    if (!this.props.trackChanges) return super.onTextInputShim(event)
    // Text Input Bindings
    var hook = this.editorSession.keyboardManager.textinputBindings[event.data]
    if (hook) {
      setTimeout(() => {
        var params = this.editorSession.keyboardManager._getParams()
        return hook(params, this.context)
      })
      return
    }
    if (selection.type === 'node') super._handleDeleteKey(event)

    return this.handleTracking({
      event,
      status: 'add',
      surfaceEvent: 'input',
      keypress: true,
    })
  }

  _handleDeleteKey(event) {
    const selection = this.context.editorSession.getSelection()
    if (!this.props.trackChanges || selection.type === 'node') {
      return super._handleDeleteKey(event)
    }

    this.deleteKey = true
    return this.handleTracking({
      event,
      parentFunc: args => {
        super._handleDeleteKey(args)
      },
      status: 'delete',
      surfaceEvent: 'delete',
    })
  }

  _handleEnterKey(event) {
    const { findAndReplaceProvider } = this.context
    if (findAndReplaceProvider.entries.length > 0) {
      event.preventDefault()
      return findAndReplaceProvider.findNext()
    }

    if (!this.props.trackChanges) return super._handleEnterKey(event)

    return this.handleTracking({
      event,
      parentFunc: args => {
        super._handleEnterKey(args)
      },
      status: 'enter',
      surfaceEvent: 'enter',
    })
  }

  _handleSpaceKey(event) {
    if (!this.props.trackChanges) return super._handleSpaceKey(event)

    return this.handleTracking({
      event,
      status: 'add',
      surfaceEvent: 'space',
    })
  }

  _onPaste(event) {
    if (!this.props.trackChanges) return super._onPaste(event)
    const { clipboardData } = event
    const types = {}
    for (let i = 0; i < clipboardData.types.length; i++) {
      types[clipboardData.types[i]] = true
    }
    event.preventDefault()
    event.stopPropagation()

    let plainText
    let html
    let SubstanceClipboard

    if (types['text/plain']) {
      plainText = clipboardData.getData('text/plain')
    }
    if (types['text/html']) {
      html = clipboardData.getData('text/html')
      SubstanceClipboard = html.includes('substance-clipboard')
    }

    const htmlImporter = this.clipboard._getImporter()
    const data = html ? html : plainText

    const content = htmlImporter.importDocument(data)
    return this.handleTracking({
      event,
      status: 'paste',
      surfaceEvent: 'paste',
      content,
      SubstanceClipboard,
    })
  }

  _onCut(event) {
    if (!this.props.trackChanges) return super._onCut(event)
    const { trackChangesProvider } = this.context
    event.preventDefault()
    this._onCopy(event)

    const options = {
      selection: this.editorSession.getSelection(),
      user: this.context.editor.props.user,
      editorSession: this.editorSession,
      surface: this,
    }
    trackChangesProvider.markNonCollapsedSelectionAsDeleted(options)
  }

  shouldIgnoreKeypress(event) {
    // see Surface's onTextInputShim for comments
    if (
      event.which === 0 ||
      event.charCode === 0 ||
      event.keyCode === keys.TAB ||
      event.keyCode === keys.ESCAPE ||
      Boolean(event.metaKey) ||
      Boolean(event.ctrlKey) ^ Boolean(event.altKey)
    ) {
      return true
    }

    return false
  }

  getTextFromKeypress(event) {
    let character = String.fromCharCode(event.which)
    if (!event.shiftKey) character = character.toLowerCase()
    if (character.length === 0) return null
    return character
  }

  handleTracking(options) {
    const { trackChangesProvider } = this.context
    const { event, keypress, surfaceEvent } = options

    if (!keypress) {
      event.preventDefault()
      event.stopPropagation()
    }

    if (surfaceEvent === 'input') {
      if (keypress) {
        if (this.shouldIgnoreKeypress(event)) return
        const text = this.getTextFromKeypress(event)
        event.data = text
        event.preventDefault()
        event.stopPropagation()
      }

      if (!keypress && !event.data) return
      this._state.skipNextObservation = true
    }

    if (surfaceEvent === 'delete') {
      const direction =
        event.keyCode === keyboardKeys.BACKSPACE ? 'left' : 'right'
      options.move = direction
      options.key = direction === 'left' ? 'BACKSPACE' : 'DELETE'
    }

    trackChangesProvider.handleTransaction(options)
  }

  //TODO BUG: Substance sends the change multiple times
  // so the anno is create equal times. Add the funcionality in
  // each pkg instead if trackChanges is on
  checkChange(change) {
    if (!this.props.trackChanges) return

    const { trackChangesProvider } = this.context

    // Just for example purposes track 'emphasis, subscript, superscript' formating
    const allowedFormating = [
      'emphasis',
      'strong',
      'subscript',
      'superscript',
      'small-caps',
    ]
    const formatingCreated = values(change.created).filter(value => value.type)
    const formatingDeleted = values(change.deleted).filter(value => value.type)
    if (
      formatingCreated.length !== 0 &&
      allowedFormating.includes(formatingCreated[0].type)
    ) {
      trackChangesProvider.trackCreatedFormatingInText(formatingCreated)
    }

    if (
      formatingDeleted.length !== 0 &&
      allowedFormating.includes(formatingDeleted[0].type)
    ) {
      trackChangesProvider.trackDeletedFormatingInText(formatingDeleted)
    }

    if (change.ops[0].type === 'set') {
      trackChangesProvider.trackExpandedFormatingInText()
    }
  }
}

export default ContainerEditor
