import { eq, get, isEmpty, isEqual, groupBy } from 'lodash'
import React from 'react'
import ReactDOM from 'react-dom'
import PropTypes from 'prop-types'
import { withRouter } from 'react-router-dom'
// TODO -- import a config builder instead of the provided config
import {
  config,
  Editor,
  Exporter,
  Importer,
  Configurator,
  EditorSession,
} from 'wax-editor-core'

import './Wax.scss'

const noop = () => {
  console.log('This is a default noop function')
  return new Promise(resolve => {
    resolve()
  })
}

class Wax extends React.Component {
  constructor(props) {
    super(props)

    this.onClose = this.onClose.bind(this)
    this.save = this.save.bind(this)
    this.focusAlwaysOnEditor = this.focusAlwaysOnEditor.bind(this)
  }

  componentWillReceiveProps(nextProps) {
    const {
      chapterNumber,
      content,
      editing,
      trackChanges,
      user,
      mode,
    } = this.props

    const newEditorProps = {}

    if (nextProps.trackChanges !== trackChanges) {
      Object.assign(newEditorProps, {
        trackChanges: nextProps.trackChanges,
      })
    }

    if (!isEqual(nextProps.user, user)) {
      Object.assign(newEditorProps, {
        user: nextProps.user,
      })
    }

    if (!isEqual(nextProps.mode, mode)) {
      Object.assign(newEditorProps, {
        mode: nextProps.mode,
      })
    }

    if (nextProps.editing !== editing) {
      Object.assign(newEditorProps, {
        editing: nextProps.editing,
      })
    }

    if (!eq(nextProps.chapterNumber, chapterNumber)) {
      Object.assign(newEditorProps, {
        chapterNumber: nextProps.chapterNumber,
      })
    }

    if (isEmpty(newEditorProps)) return
    this.editor.extendProps(newEditorProps)
  }

  createSession(content = '') {
    // Make sure that any existing editorsession gets disposed properly
    if (get(this.editor, 'editorSession')) {
      this.editor.editorSession.document.dispose()
      this.editor.editorSession.dispose()
    }

    const importer = this.configurator.createImporter('html')
    const doc = importer.importDocument(content)

    const editorSession = new EditorSession(doc, {
      configurator: this.configurator,
    })

    editorSession.setSaveHandler({
      saveDocument: this.save,
      uploadFile: this.props.fileUpload,
    })

    return editorSession
  }

  save(source, changes, callback) {
    const { onSave } = this.props
    const { config } = this.state.config
    const exporter = new Exporter(config)
    const convertedSource = exporter.exportDocument(source)

    return onSave(convertedSource)
  }

  mountEditor() {
    const el = ReactDOM.findDOMNode(this)

    const {
      content,
      chapterNumber,
      customTags,
      editing,
      layout,
      onSave,
      trackChanges,
      update,
      user,
      autoSave,
      mode,
      menus,
      theme,
    } = this.props

    this.configurator = new Configurator().import(config)
    this.configurator.addImporter('html', Importer)
    this.configurator.menus = menus
    const editorSession = this.createSession(content)

    const onClose = this.onClose
    // TODO -- make this configurable
    const containerId = 'main'

    this.setState({
      config: this.configurator,
    })

    const groupedCustomTags = groupBy(customTags,'tagType')

    this.editor = Editor.mount(
      {
        configurator: this.configurator,
        containerId,
        chapterNumber,
        customTags: groupedCustomTags,
        editing,
        editorSession,
        layout,
        onClose,
        onSave,
        trackChanges,
        update,
        user,
        mode,
        autoSave: autoSave || false,
        theme: theme || 'editoria',
      },
      el,
    )
  }

  componentDidMount() {
    this.mountEditor()
    this.setRouteLeaveHook()

    const el = ReactDOM.findDOMNode(this.node)
    el.addEventListener('click', this.focusAlwaysOnEditor, this)
  }

  componentWillUnmount() {
    this.editor.editorSession.document.dispose()
    this.editor.triggerDispose()
  }

  // TODO -- move to core
  focusAlwaysOnEditor(e) {
    // Allow losing focus when another (not the editor) editable area is focused
    const targetElementName = e.target.tagName.toUpperCase()
    let targetElement = e.target

    // Check target elements parentNode to check if click is allowed.
    while (
      targetElement &&
      typeof targetElement.getAttribute === 'function' &&
      targetElement.getAttribute('data-clickable') === null
    ) {
      targetElement = targetElement.parentNode
    }
    if (
      (targetElement &&
        typeof targetElement.getAttribute === 'function' &&
        targetElement.getAttribute('data-clickable') !== true) ||
      targetElementName === 'TEXTAREA' ||
      targetElementName === 'INPUT'
    )
      return

    // TODO -- update when 'main' is configurable
    // Wherever 'main' is hardcoded should come from config.
    const containerId = 'main'

    // Get surface coordinates
    const surfaceElement = document.querySelector(
      `[data-surface-id="${containerId}"]`,
    )
    const rectSurface = surfaceElement.getBoundingClientRect()

    const contentPanelElement = document.getElementById(
      `content-panel-${containerId}`,
    )
    const rectPanel = contentPanelElement.getBoundingClientRect()

    const bottom =
      rectSurface.bottom > rectPanel.bottom
        ? rectPanel.bottom
        : rectSurface.bottom

    // The combination of the surface's width and the content panel's height
    // gives us the coordinates of the visible surface

    // If click is outside the surface
    if (
      e.clientX < rectSurface.left ||
      e.clientX > rectSurface.right ||
      e.clientY < rectSurface.top ||
      e.clientY > bottom
    ) {
      const { editorSession } = this.editor
      const surface = editorSession.surfaceManager.getSurface(containerId)

      // If there is no selection, simply put the focus at the beginning of the doc
      if (editorSession.getSelection().isNull()) {
        surface.setFocusOnStartOfText()
        return
      }

      // If there is a selection, recreate its last position
      setTimeout(() => {
        editorSession.setSelection(editorSession.getSelection())
      })
    }
  }

  onClose(nextRoute, action) {
    if (action === 'POP') {
      this.props.history.go(-1)
    } else {
      this.props.history.push(nextRoute)
    }
  }

  // TODO -- use Prompt instead
  setRouteLeaveHook() {
    const { history } = this.props

    history.block((location, action) => {
      if (this.editor.editorSession.hasUnsavedChanges()) {
        this.editor.triggerUnsavedModal(location.pathname, action)
        return false
      }
      return true
    })
  }

  render() {
    return (
      <div className={this.props.className} ref={node => (this.node = node)} />
    )
  }
}

Wax.propTypes = {
  className: PropTypes.string,
  content: PropTypes.string,
  chapterNumber: PropTypes.number,
  editing: PropTypes.string,
  fileUpload: PropTypes.func,
  history: PropTypes.any,
  layout: PropTypes.string,
  onSave: PropTypes.func,
  update: PropTypes.func,
  user: PropTypes.object,
}

Wax.defaultProps = {
  className: 'editor-wrapper',
  content: '',
  chapterNumber: NaN,
  editing: 'full',
  mode: {
    trackChanges: {
      toggle: true,
      view: true,
      own: {
        accept: true,
        reject: true,
      },
      others: {
        accept: true,
        reject: true,
      },
    },
    styling: true,
  },
  fileUpload: noop,
  layout: 'default',
  onSave: noop,
  readOnly: false,
  trackChanges: false,
  update: noop,
  user: {
    id: 1,
    roles: ['admin'],
    username: 'demo',
    color: {
      addition: '#4990e2',
      deletion: '#c00',
    },
  },
}

export default withRouter(Wax)
