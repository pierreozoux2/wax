# Wax Editor

This application is being developed by the [Coko Foundation](https://coko.foundation/), for the [University of California Press](http://www.ucpress.edu/) as part of the [Editoria](https://gitlab.coko.foundation/editoria/editoria/) application.

Wax Editor is build against Substance libraries. Check Substance [website](http://substance.io/) and [GitHub repo](https://github.com/substance/substance) for more information. 

## Table of Contents
- [Get up and running](#get-up-and-running)
- [Features](#features)
    - [Annotation Tools](#annotation-tools)
    - [Block Level Styles](#block-level-styles)
    - [Change Case](#change-case)
    - [Image With Caption](#image-with-caption)
    - [Notes](#notes)
    - [Lists](#lists)
    - [Comments](#comments)
    - [Highlighter](#highlighter)
    - [Ornament](#ornament)
    - [Spell Checker](#spell-checker)
    - [Code Block](#code-block)
    - [Full Screen](#full-screen)
    - [Keyboard Shortcuts](#keyboard-shortcuts)
    - [Track Changes](#track-changes)

## Get up and running

Check a demo of the [Editor](http://wax-demo.coko.foundation/)

Run a local standalone version
  
1)  `git clone git@gitlab.coko.foundation:wax/wax-react.git`
  
2) `yarn`
  
3) `yarn start`

## Features

### Annotation Tools

Annotation tools include: 
*  Strong
*  Emphasis
*  Small caps
*  Super Script
*  Sub Script
*  Inline Code
*  Link

### Block Level Styles

Block Level Styles include:

*  Title (Only One can be marked per Chapter)
*  Author
*  Epigraph Prose
*  Epigraph Poetry
*  Source Note
*  Headings 1,2,3
*  Paragraph
*  Paragraph Continued
*  Extract Prose
*  Extract Poetry
*  Bibliography Entry

### Change Case
Transforms a selection to:

*  Upper Case
*  Lower Case
*  Sentence Case
*  Title Case


### Image With Caption

Wax supports image upload and adding a caption. On the caption **emphasis** and **small caps** are supported along with **comments**, **find and replace** and **track changes**.
Adding a new paragraph below the image simply click on the image and hit enter.

### Notes

Adding a note to the main text will create a designated area at the bottom of the editor for adding note content.
  
Adding a new note or dragging and dropping a note callout will automatically renumber both all the note callouts in the main text as their corresponding text.
  
Clicking on a note callout will transfer the focus to the editing area. Clicking on a note number in the note editing are will find the note in the main text.
  
Notes editing area can be dragged in order to work better on notes.
  
Comments and track changes are supported for notes.
  
### Lists

Adding a Bullet or Number list. Lists can be nested. Hitting TAB will indent the list, and Shift + TAB will dedent the list. 

### Comments
Selecting a part of a text a comment icon will appear on the right of the editing surface. it will mark the text as comment ,and a comment box will appear on the right which prompts you to 
enter the comment actual text. In order for the comment to be saved either enter has to be pressed or clickcing to the post comment icon in the bottom of the comment box.

### Special Characters

A modal that allows insertion of Unicode characters into the document.

### Find And Replace

Will search for an exact match in main text, image captions and notes. You can either replace all, replace current selected match or find next/previous. Hitting enter while there are matches will highlight
the next available match. Also works with track changes on (will track-delete the match and add a new addition) .Shortcut: `ctlr + f`
### Highlighter

Highlights the selected text in either blue/red or yellow color.

### Ornament

Inserts a seperator block.

### Spell Checker

### Code Block
Will inject an instance of CodeMirror [(https://codemirror.net/](https://codemirror.net/)) where you can write/paste code bloks in Javascript, HTML or CSS. 
### Full Screen

Will toggle between having the editor in full screen and default.

### Keyboard shortcuts

List of all available shortcuts.

### Track Changes